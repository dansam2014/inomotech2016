<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSermonSummaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sermon_summaries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('summary_id',8);
            $table->string('summary_title');
            $table->string('preacher');
            $table->text('summary_content');
            $table->string('pic')->nullable();
            $table->string('publish',1)->default('1');
            $table->string('sermon_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sermon_summaries');
    }
}
