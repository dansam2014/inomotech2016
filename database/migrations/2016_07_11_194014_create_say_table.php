<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
          Schema::create('says', function (Blueprint $table) {
            $table->increments('id');
            $table->string('say_id',8);
            $table->string('say_name');
             $table->string('say_content');
            $table->string('say_image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('says');
    }
}
