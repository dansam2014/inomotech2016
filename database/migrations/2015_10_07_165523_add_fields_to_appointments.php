<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToAppointments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appointments', function (Blueprint $table) {
            $table->string('user_id')->after('appointment_id');
            $table->date('desired_date')->nullable();
            $table->string('desired_time')->nullable();
            $table->date('given_date')->nullable();
            $table->string('given_time')->nullable();
            $table->boolean('confirmed')->default(0);
            $table->dropColumn('desired_period');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appointments', function (Blueprint $table) {
            $table->dropColumn('desired_date');
            $table->dropColumn('desired_time');
            $table->dropColumn('given_date');
            $table->dropColumn('given_time');
            $table->dropColumn('confirmed');
        });
    }
}
