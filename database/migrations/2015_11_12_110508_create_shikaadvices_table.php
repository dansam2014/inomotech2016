<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShikaadvicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shika_advices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('fullname',70);
            $table->string('contact',20);
            $table->string('email',30);
            $table->string('subject');
            $table->text('message');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shika_advices');
    }
}
