<div class="container-fluid">
                <nav class="navbar navbar-default yamm">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ asset('main/img/logo.png')}}" alt=""></a>
                        </div>

                        <div id="navbar" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <li><a href="{{ url('/')}}" class="active">Home </a> </li>
                                <li><a href="{{ url('about') }}">About</a></li>
                                <li><a href="{{ url('clients') }}">Clients</a></li>
                                <li><a href="{{ url('services') }}">Services</a></li>
                                <li><a href="{{ url('contact') }}">Contact</a></li>
                                <li><a href="{{ url('blogs') }}">Blogs</a></li>
                                <li class="search-top">

                               {!! Form::open(array('url'=>'search','name'=>'s','files'=>true)) !!}
                                    <div class="input-append">
                                         {!! Form::text('search','',array('class'=>'form-control','placeholder'=>'Search...')) !!}
                                    </div>
                                {!! Form::close() !!}
                                </li>

                            </ul>

                            {{--<ul class="nav navbar-nav navbar-right hidden-xs">--}}
                                {{--<li class="dropdown searchmenu hasmenu">--}}
                                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Sign In <i class="fa fa-angle-down"></i></a>--}}
                                    {{--<ul class="dropdown-menu show-right">--}}
                                        {{--<li>--}}
                                            {{--<div id="custom-search-input">--}}
                                                {{--<div class="input-group col-md-12">--}}
                                                    {{--<input type="text" class="form-control input-lg" placeholder="User name" />--}}
                                                    {{--<input type="password" class="form-control input-lg" placeholder="Password" />--}}
                                                    {{--<label><a href="#">Forget Password?</a></label>--}}
                                                    {{--<button class="btn btn-primary btn-block">Login Account</button>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        </div><!--/.nav-collapse -->
                    </div><!--/.container-fluid -->
                </nav><!-- end nav -->
            </div>