<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <ul class="list-inline text-center">
            @foreach($policy as $policy_all)
                <li><a href="/page/{{ $policy_all->policy_id }}">{{ $policy_all->policy_name }}</a></li>
            @endforeach
            </ul>
        </div>
    </div><!-- end row -->
</div><!-- end container -->