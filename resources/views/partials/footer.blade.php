 <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h4>Quick Links</h4>
                            </div><!-- end widget-title -->

                            <div class="link-widget">   
                                <ul class="check">
                                    <li><a href="{{ url('/') }}">Home</a></li>
                                    <li><a href="{{ url('about') }}">About Us</a></li>
                                    <li><a href="{{ url('contact') }}">Contact Us</a></li>
                                    <li><a href="{{ url('clients') }}">Our Clients</a></li>
                                    <li><a href="{{ url('services') }}">Services</a></li>
                                </ul><!-- end check -->
                            </div><!-- end link-widget -->
                        </div>
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-12">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h4>Address</h4>
                            </div><!-- end widget-title -->

                            <div class="link-widget">   
                                <ul class="address-footer">
                                    <p>P.O.Box</p>
                                    <p>KN, 4023</p>
                                    <p>Accra-Ghana</p>

                                </ul><!-- end check -->
                            </div><!-- end link-widget -->
                        </div>
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-12">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h4>Information</h4>
                            </div><!-- end widget-title -->
                            <div class="link-widget">   
                                <ul class="address-footer">
                                   <p>Call: +233-244-139-937</p>
                                   <p>Call: +233-248-755-510</p>
                                   <p>Email: <a href="mailto:hello@inomotech.com" style="color:#FFF;">hello@inomotech.com</a> </p>
                                </ul><!-- end check -->
                            </div><!-- end link-widget -->
                        </div>

                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-12">
                        <div class="widget clearfix">
                            <div class="widget-title ">
                                <h4>About Us</h4>
                            </div><!-- end widget-title -->
                            <div class="address-footer">
                            <p>We are known to provide the most effective and efficient software application
                                                    that ensure maximum security and expedite business transactions in Africa</p>
                            </div>


                        </div>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->