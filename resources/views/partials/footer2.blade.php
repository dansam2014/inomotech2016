<div class="footer-distributed">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12 footer-left">
                        <div class="widget">
                        <img src="images/flogo.png" alt="">
                        <p class="footer-links">
                            <a href="#">Home</a>
                            ·
                            <a href="#">Blog</a>
                            ·
                            <a href="#">Pricing</a>
                            ·
                            <a href="#">About</a>
                            ·
                            <a href="#">Faq</a>
                            ·
                            <a href="#">Contact</a>
                        </p>
                        <p class="footer-company-name">Company Name &copy; 2015</p>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12 footer-center">
                        <div class="widget">
                        <div>
                            <i class="fa fa-map-marker"></i>
                            <p>21 Revolution Street Paris, France</p>
                        </div>
                        <div>
                            <i class="fa fa-phone"></i>
                            <p>+1 555 123456</p>
                        </div>
                        <div>
                            <i class="fa fa-envelope-o"></i>
                            <p><a href="mailto:support@company.com">support@company.com</a></p>
                        </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12 footer-right">
                        <div class="widget">
                        <p class="footer-company-about">
                            <span>About the company</span>
                            Lorem ipsum dolor sit amet, consectateur adispicing elit. Fusce euismod convallis velit, eut.
                        </p>
                        <div class="footer-icons">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                            <a href="#"><i class="fa fa-github"></i></a>
                        </div>
                        </div>
                    </div>
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end copyrights -->

        <div class="chat-wrapper">
            <div class="panel panel-primary">
                <div class="panel-heading" id="accordion">
                    <a class="btn btn-primary btn-block btn-xs" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                        <span class="fa fa-comments-o"></span> Customer Support
                    </a>
                </div>
                <div class="panel-collapse collapse" id="collapseOne">
                    <div class="panel-body">
                        <ul class="chat">
                            <li class="left clearfix">
                                <span class="chat-img">
                                    <img src="upload/client_01.png" alt="User Avatar" class="img-circle img-responsive alignleft" />
                                </span>
                                <div class="chat-body clearfix">
                                    <div class="chat-header">
                                        <strong class="primary-font">John DOE</strong> <small class="pull-right text-muted">
                                        <span class="fa fa-clock-o"></span>12 mins ago</small>
                                    </div>
                                    <p>Hello anyone here? I need to purchase web hosting!</p>
                                </div>
                            </li>
                      
                            <li class="left clearfix">
                                <span class="chat-img">
                                    <img src="upload/client_01.png" alt="User Avatar" class="img-circle img-responsive alignleft" />
                                </span>
                                <div class="chat-body clearfix">
                                    <div class="chat-header">
                                        <strong class="primary-font supportstaff">Staff</strong> <small class="pull-right text-muted">
                                        <span class="fa fa-clock-o"></span>13 mins ago</small>
                                    </div>
                                    <p>Hey John! Welcome to the HostHubs support chat!</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="panel-footer">
                        <div class="input-group">
                            <input id="btn-input" type="text" class="form-control input-sm" placeholder="Type your message here..." />
                            <span class="input-group-btn">
                                <button class="btn btn-default btn-sm" id="btn-chat">Send</button>
                            </span>
                        </div>
                    </div><!-- end panel-footer -->
                </div><!-- end panel-collapse -->
            </div><!-- end panel -->
        </div><!-- end chat-wrapper -->
