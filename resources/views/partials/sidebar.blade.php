 <div class="widget">
        <div class="loginbox text-center">
            <h3>Subscribe Today!</h3>
            <p>Subscribe to our Newsletter and receive updates via email.</p>

                 {!! Form::open(array('url'=>'news', 'class'=>'hero-newsletter-form','method'=>'post','files'=>true)) !!}

                     @if( ($errors->any()))
                         <ul class="alert alert-danger">
                             @foreach ($errors->all() as $error)
                                 <li class="error_message"> {{ $error }}</li>
                             @endforeach
                         </ul>
                     @endif


                     @if (session()->has('message'))
                         <div class="alert alert-success">
                         {{ session('message') }}
                         </div>
                     @endif
                          <input type="email" name="email" class="form-control" placeholder="Enter your email here..">
                        <input type="submit" value="Subscribe" class="btn btn-primary btn-block" />

                 {!! Form::close() !!}
        </div><!-- end newsletter -->
    </div><!-- end widget -->

    <div class="widget">
        <div class="wbp">
            <div class="small-title">
            <h3>Latest from our blog</h3>
            <hr>
            </div>

            <div class="related-posts">

                @foreach($blogs1 as $side)
                <div class="entry">
                    <p><a href="{{ url('blogs/'.$side->post_id) }}" title=""> {{ $side->post_title }}</a></p>
                    <small>{{ $side->created_at }}</small>
                </div><!-- end entry -->
                @endforeach


            </div><!-- end related -->
        </div><!-- end newsletter -->
    </div><!-- end widget -->