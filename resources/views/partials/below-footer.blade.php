<div class="footer-distributed">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12 footer-left">
                        <div class="widget">
                        <img src="{{ asset('main/img/l.png') }}" alt="">
                        <p class="footer-links">
                        @foreach($policy as $policy_all)
                            <a href="/page/{{ $policy_all->policy_id }}">{{ $policy_all->policy_name }}</a>
                            |
                        @endforeach

                        </p>

                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12 footer-center">
                        <div class="widget">
                        <div>
                            <i class="fa fa-map-marker"></i>
                            <p class="footer-company-name"> Copyright &copy; 2016,</p>
                            <p class="footer-company-name"> All rights reserved</p>
                        </div>

                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12 footer-right">
                        <div class="widget">
                        <p class="footer-company-about">
                            <span>Connect on Social Media</span>
                        <div class="footer-icons">
                            <a href="https://www.facebook.com/Inomotech" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="https://twitter.com/inomotech" target="_blank"><i class="fa fa-twitter"></i></a>
                            {{--<a href="#"><i class="fa fa-linkedin"></i></a>--}}
                            {{--<a href="#"><i class="fa fa-github"></i></a>--}}
                        </div>
                        </div>
                    </div>
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end copyrights -->

        <div class="chat-wrapper">
            <div class="panel panel-primary">
                <div class="panel-heading" id="accordion">
                    <a class="btn btn-primary btn-block btn-xs" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                        <span class="fa fa-comments-o"></span> Customer Support
                    </a>
                </div>
                <div class="panel-collapse collapse" id="collapseOne">
                {!! Form::open(array('url'=>'customerSupport','method'=>'post','files'=>true)) !!}
                    <div class="panel-body">
                        <ul class="chat">
                            <li class="left clearfix">

                                 @if( ($errors->any()))
                                     <ul class="alert alert-danger">
                                         @foreach ($errors->all() as $error)
                                             <li class="error_message"> {{ $error }}</li>
                                         @endforeach
                                     </ul>
                                 @endif


                                 @if (session()->has('message'))
                                     <div class="alert alert-success">
                                     {{ session('message') }}
                                     </div>
                                 @endif
                                 <div class="chat-body clearfix">
                                    <div class="chat-header">
                                        <strong class="primary-font">Full name</strong> <small class="pull-right text-muted"></small>
                                        {!! Form::text('fullname','',array('class'=>'form-control required','id'=>'name','placeholder'=>'Name')) !!}

                                    </div>
                                </div>
                            </li>

                            <li class="left clearfix">
                                <div class="chat-body clearfix">
                                    <div class="chat-header">
                                        <strong class="primary-font">Phone Number</strong> <small class="pull-right text-muted"></small>
                                       {!! Form::text('phone','',array('class'=>'form-control required','id'=>'name','placeholder'=>'0244139937')) !!}
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="panel-footer">
                        <div class="input-group">
                            {!! Form::text('message','',array('class'=>'form-control required','id'=>'name','placeholder'=>'Type message here ...')) !!}
                            <span class="input-group-btn">
                                {{--<button class="btn btn-default btn-sm">Send</button>--}}
                                {!! Form::submit('Send',array('class'=>'btn btn-default btn-sm')) !!}
                            </span>
                        </div>
                    </div><!-- end panel-footer -->
                {!! Form::close() !!}
                </div><!-- end panel-collapse -->
            </div><!-- end panel -->
        </div><!-- end chat-wrapper -->