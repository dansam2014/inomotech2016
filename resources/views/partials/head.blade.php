    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- SITE META -->
    <title>Innovative Modern Technology</title>
    <meta name="description" content="Innovative Modern Technology is a technology giant in Africa. We provide the most effective and efficient software application that ensure maximum security and expedite business transactions. Our greatest concern is to provide our clients the best services. " />
    <meta name="keywords" content="IMT, imt, inomotech,INOMOTECH,technology, computer, software, hardware, website, web application, mobile, mobile application, softare testing , testing company, software quality, software quality assurance, software quality assurance testing, business automation, IT consultancy, consultant, consultancy, IT, information, technogy, websites" />
    <meta name="author" content="Innovative Modern Technology">

    <!-- FAVICONS -->
    <link rel="shortcut icon" href="{{ asset('main/upload/favicon.png') }}" type="image/x-icon">

    <!-- REVOLUTION STYLE SHEETS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('main/revolution/css/settings.css') }}">
    <!-- REVOLUTION LAYERS STYLES -->
    <link rel="stylesheet" type="text/css" href="{{ asset('main/revolution/css/layers.css') }}">
    <!-- REVOLUTION NAVIGATION STYLES -->
    <link rel="stylesheet" type="text/css" href="{{ asset('main/revolution/css/navigation.css')}}">

    <!-- BOOTSTRAP STYLES -->
    <link rel="stylesheet" type="text/css" href="{{ asset('main/css/bootstrap.css')}}">
    <!-- TEMPLATE STYLES -->
    <link rel="stylesheet" type="text/css" href="{{ asset('main/css/style.css') }}">
    <!-- RESPONSIVE STYLES -->
    <link rel="stylesheet" type="text/css" href="{{ asset('main/css/responsive.css') }}">
    <!-- CUSTOM STYLES -->
    <link rel="stylesheet" type="text/css" href="{{ asset('main/css/custom.css') }}">

    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-53454950-1', 'auto');
      ga('send', 'pageview');

    </script>
