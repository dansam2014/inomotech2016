<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->


<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    @include('partials/head')
</head>
<body>

	<!-- PRELOADER -->
        <div id="loader">
			<div class="loader-container">
				<img src="{{ asset('main/images/load.gif')}}" alt="" class="loader-site spinner">
			</div>
		</div>
	<!-- END PRELOADER -->

    <div id="wrapper">
        <div class="topbar">
            <div class="container">
                @include('partials/top-header')
            </div><!-- end topbar -->
        </div><!-- end topbar -->

        <header class="header">
            @include('partials/header')<!-- end container -->
        </header><!-- end header -->

        <div class="after-header">
            @include('partials/below-header')
        </div><!-- end after-header -->

        <div class="first-slider">
            <div id="rev_slider_211_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="webproductlighthero" style="background-color:#1D4BAD;margin:0px auto;padding:0px;margin-top:0px;margin-bottom:0px;">
                <!-- START REVOLUTION SLIDER 5.1.1RC fullwidth mode -->
                <div id="rev_slider_211_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.1.1RC">
                    <ul>
                        <!-- SLIDE  -->
                        <li data-index="rs-699" data-transition="fade" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="1500" data-rotate="0" data-saveperformance="off" data-title="Intro" data-description="">
                            <!-- MAIN IMAGE -->
                            <img src="{{ asset('main/upload/transparent.png')}}" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption   tp-resizeme rs-parallaxlevel-6" id="slide-699-layer-1" data-x="['right','right','center','center']" data-hoffset="['-254','-453','70','60']" data-y="['middle','middle','middle','bottom']" data-voffset="['30','50','211','25']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:right;s:1500;e:Power3.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-start="2500" data-responsive_offset="on" style="z-index: 5;">
                              <img src="{{ asset('main/upload/macbookpro.png')  }}" alt="" width="1000" height="600" data-ww="['1000px','1000px','500px','350px']" data-hh="['600px','600px','300px','210px']" data-no-retina>
                            </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption   tp-resizeme rs-parallaxlevel-6 tp-videolayer" id="slide-699-layer-11" data-x="['left','left','left','left']" data-hoffset="['829','866','297','185']" data-y="['top','top','top','top']" data-voffset="['197','217','582','514']" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:2;sY:2;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                            data-start="3350" data-responsive_offset="on" data-videocontrols="none" data-videowidth="['653px','653px','325px','229px']" data-videoheight="['408px','408px','204px','145px']" data-videoposter="/main/upload/traincover.jpg" data-videomp4="/main/upload/Broadway.mp4" data-posterOnMObile="off" data-videopreload="auto" data-videoloop="loop" data-autoplay="on" style="z-index: 6;">
                            </div>

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption   tp-resizeme rs-parallaxlevel-5" id="slide-699-layer-12" data-x="['left','left','left','left']" data-hoffset="['543','543','110','155']" data-y="['top','top','top','top']" data-voffset="['17','17','500','474']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="opacity:0;s:300;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="3950" data-responsive_offset="on" style="z-index: 7;">
                                <div class="rs-looped rs-slideloop" data-easing="Linear.easeNone" data-speed="30" data-xs="-600" data-xe="600" data-ys="0" data-ye="0">
                                <img src="{{ asset('main/upload/cloud1.png')  }}" alt="" width="500" height="273" data-ww="['500px','500px','250','125px']" data-hh="['273px','273px','137','68px']" data-no-retina>
                                </div>
                            </div>

                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption   tp-resizeme rs-parallaxlevel-4" id="slide-699-layer-3" data-x="['left','left','center','center']" data-hoffset="['593','633','-110','-60']" data-y="['top','top','top','bottom']" data-voffset="['183','203','590','20']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:right;s:1500;e:Power3.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-start="2750" data-responsive_offset="on" style="z-index: 8;">
                                <img src="{{ asset('main/upload/ipad.png') }}" alt="" width="430" height="540" data-ww="['430px','430px','200px','170px']" data-hh="['540px','540px','251px','213px']" data-no-retina>
                            </div>

                            <!-- LAYER NR. 5 -->
                            <div class="tp-caption   tp-resizeme rs-parallaxlevel-4" id="slide-699-layer-4" data-x="['left','left','left','center']" data-hoffset="['663','703','212','-60']" data-y="['top','top','top','bottom']" data-voffset="['271','291','632','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-start="3700" data-responsive_offset="on" style="z-index: 9;">
                                <img src="main/upload/express_ipad_content1.jpg" alt="" width="290" height="374" data-ww="['290px','290px','135px','115px']" data-hh="['374px','374px','174px','148px']" data-no-retina>
                            </div>

                            <!-- LAYER NR. 6 -->
                            <div class="tp-caption   tp-resizeme rs-parallaxlevel-3" id="slide-699-layer-13" data-x="['left','left','left','left']" data-hoffset="['294','294','116','97']" data-y="['top','top','top','top']" data-voffset="['532','532','745','641']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="opacity:0;s:300;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="3950" data-responsive_offset="on" style="z-index: 10;">
                                <div class="rs-looped rs-slideloop" data-easing="Linear.easeNone" data-speed="30" data-xs="-400" data-xe="400" data-ys="0" data-ye="0">
                                    <img src="{{ asset('main/upload/cloud2.png') }}" alt="" width="600" height="278" data-ww="['600px','600px','300','150']" data-hh="['278px','278px','139','70']" data-no-retina>
                                </div>
                            </div>

                            <!-- LAYER NR. 7 -->
                            <div class="tp-caption   tp-resizeme rs-parallaxlevel-2" id="slide-699-layer-5" data-x="['left','left','left','left']" data-hoffset="['530','553','127','58']" data-y="['top','top','top','top']" data-voffset="['278','297','622','529']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:right;s:1500;e:Power3.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-start="3000" data-responsive_offset="on" style="z-index: 11;">
                                <img src="{{ asset('main/upload/ihpone.png') }}" alt="" width="260" height="450" data-ww="['260px','260px','130px','100px']" data-hh="['450px','450px','225px','173px']" data-no-retina>
                            </div>

                            <!-- LAYER NR. 8 -->
                            <div class="tp-caption   tp-resizeme rs-parallaxlevel-2" id="slide-699-layer-6" data-x="['left','left','left','left']" data-hoffset="['576','598','150','75']" data-y="['top','top','top','top']" data-voffset="['360','379','663','560']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-start="3950" data-responsive_offset="on" style="z-index: 12;">
                                <img src="main/upload/express_iphone_content1.jpg" alt="" width="170" height="286" data-ww="['170px','170px','85px','66px']" data-hh="['286px','286px','143px','111px']" data-no-retina>
                            </div>

                            <!-- LAYER NR. 9 -->
                            <div class="tp-caption   tp-resizeme rs-parallaxlevel-1" id="slide-699-layer-14" data-x="['left','left','left','left']" data-hoffset="['280','280','-10','-1']" data-y="['top','top','top','top']" data-voffset="['223','223','569','518']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="opacity:0;s:300;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="3950" data-responsive_offset="on" style="z-index: 13;">
                                <div class="rs-looped rs-slideloop" data-easing="Linear.easeNone" data-speed="30" data-xs="-200" data-xe="200" data-ys="0" data-ye="0">
                                    <img src="{{ asset('main/upload/cloud3.png')}}" alt="" width="738" height="445" data-ww="['600px','600px','300','150']" data-hh="['278px','278px','181','90']" data-no-retina>
                                </div>
                            </div>

                            <!-- LAYER NR. 10 -->
                            <div class="tp-caption WebProduct-Title   tp-resizeme rs-parallaxlevel-7" id="slide-699-layer-7" data-x="['left','left','left','left']" data-hoffset="['30','30','200','80']" data-y="['middle','middle','top','top']" data-voffset="['0','0','177','160']" data-fontsize="['90','90','75','60']" data-lineheight="['90','90','75','60']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:-50px;opacity:0;s:1000;e:Power2.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 14; white-space: nowrap;">Responsive
                                <br/> Designs!
                            </div>

                            <!-- LAYER NR. 11 -->
                            <div class="tp-caption WebProduct-Content   tp-resizeme rs-parallaxlevel-7" id="slide-699-layer-9" data-x="['left','left','left','left']" data-hoffset="['30','30','200','80']" data-y="['middle','middle','top','top']" data-voffset="['129','127','365','314']" data-fontsize="['16','16','16','14']" data-lineheight="['24','24','24','22']" data-width="['448','356','370','317']" data-height="['none','none','81','88']" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="x:-50px;opacity:0;s:1000;e:Power2.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-start="1500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 15; min-width: 448px; max-width: 448px; white-space: normal;">We provide the best 
                                <br/> and secured websites and web apps
                            </div>
                            <div class="tp-caption rev-btn rev-btn  rs-parallaxlevel-8" id="slide-699-layer-8" data-x="['left','left','left','left']" data-hoffset="['30','30','200','80']" data-y="['middle','middle','top','top']" data-voffset="['228','228','456','400']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Linear.easeNone;" data-style_hover="c:rgba(51, 51, 51, 1.00);bg:rgba(255, 255, 255, 1.00);" data-transform_in="x:-50px;opacity:0;s:1000;e:Power2.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-start="1750" data-splitin="none" data-splitout="none" data-actions='[{"event":"click","action":"scrollbelow","offset":"px"}]' data-responsive_offset="on" data-responsive="off" style="z-index: 16; white-space: nowrap; font-size: 16px; line-height: 48px; font-weight: 600; color: rgba(255, 255, 255, 1.00);font-family:Raleway;background-color:rgba(51, 51, 51, 1.00);padding:0px 40px 0px 40px;border-color:rgba(0, 0, 0, 1.00);border-width:2px;letter-spacing:1px;">GET STARTED TODAY
                            </div>
                        </li>
                    </ul>
                    <div class="tp-static-layers">
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption -   tp-static-layer" id="slide-102-layer-1" data-x="['right','right','right','right']" data-hoffset="['30','30','30','30']" data-y="['top','top','top','top']" data-voffset="['30','30','30','30']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="opacity:0;s:1000;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;" data-start="500" data-splitin="none" data-splitout="none" data-actions='[{"event":"click","action":"toggleclass","layer":"slide-102-layer-1","delay":"0","classname":"open"},{"event":"click","action":"togglelayer","layerstatus":"hidden","layer":"slide-102-layer-3","delay":"0"},{"event":"click","action":"togglelayer","layerstatus":"hidden","layer":"slide-102-layer-4","delay":"0"},{"event":"click","action":"togglelayer","layerstatus":"hidden","layer":"slide-102-layer-5","delay":"0"},{"event":"click","action":"togglelayer","layerstatus":"hidden","layer":"slide-102-layer-6","delay":"0"}]' data-basealign="slide" data-responsive_offset="off" data-responsive="off" data-startslide="-1" data-endslide="-1" style="z-index: 5; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);">
                            <div id="rev-burger">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                    </div>
                    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                </div>
            </div>
            <!-- END REVOLUTION SLIDER -->
        </div>



          <section class="section">
              <div class="container">
                  <div class="section-title text-center">
                      <h3>Service <span>Overviews</span></h3>
                      <p class="last">
                        We have been tested to be trusted
                      </p>
                  </div><!-- end section-title -->

                  <div class="row rounded-services text-center">
                      <div class="col-md-4 col-sm-6">
                          <div class="box">
                              <div class="hi-icon wow fadeIn">
                                  <i class="fa fa-laptop"></i>
                              </div>
                              <h3>Web Development</h3>
                              <p>We provide the most effective and efficient software application that ensure maximum security and expedite business transactions. Our greatest concern is to provide our clients the best services.
    </p>
                          </div><!-- end box -->
                      </div>
                      <div class="col-md-4 col-sm-6 ">
                          <div class="box">
                              <div class="hi-icon wow fadeIn">
                              <i class="fa fa-mobile"></i>
                              </div>
                              <h3>Mobile Development</h3>
                              <p>We work with industry standards to help in building mobile applications and implementation strategies. </p>
                          </div><!-- end box -->
                      </div>

                      <div class="col-md-4 col-sm-6">
                          <div class="box">
                              <div class="hi-icon wow fadeIn">
                              <i class="fa fa-level-up"></i>
                              </div>
                              <h3>ICT Advisory & Consultancy</h3>
                              <p>IMT's stalk and trade is providing innovative, cutting edge, bespoke solutions in terms of ICT advisory, consultancy and general restructuring of ICT policies and procedures of Organizations/Institutions. </p>
                          </div><!-- end box -->
                      </div><!-- end col -->
                  </div><!-- end row -->
              </div><!-- end container -->
          </section>

           <section class="section lb">
                 <div class="container">
                          <div class="row">
                              <div class="col-md-12 col-sm-12 clearfix">
                                  <div class="grid-layout">

                                     @foreach($posts as $post)
                                          <div class="content grid-blog wbp">
                                              <div class="post-media clearfix entry">
                                                  <img src="{{ $post->post_image }}" alt="" class="img-responsive">
                                                  <div class="magnifier">
                                                      <div class="visible-buttons">
                                                          <span><a data-placement="bottom" data-toggle="tooltip" title="" href="{{ url('blogs/'.$post->post_id) }}"><i class="fa fa-link"></i></a></span>
                                                      </div>
                                                  </div>
                                              </div><!-- end post-media -->

                                              <div class="post-padding clearfix">
                                                  <div class="large-post-meta">
                                                      <small>&#124;</small>
                                                      {{--<span class="hidden-xs"><a href="#"><i class="fa fa-comments-o"></i> 31</a></span>--}}
                                                      {{--<small class="hidden-xs">&#124;</small>--}}
                                                      {{--<span class="hidden-xs"><a href="single-review.html"><i class="fa fa-eye"></i> 127</a></span>--}}
                                                  </div><!-- end meta -->

                                                  <h3 class="entry-title"><a href="{{ url('blogs/'.$post->post_id) }}">{{ $post->post_title}}</a></h3>

                                                  <div class="post-sharing">
                                                      <ul class="list-inline">
                                                          <li><a href="http://www.facebook.com/sharer.php?u=http://inomotech.net/blogs/{{ $post->post_id }}" class="fb-button btn btn-primary"><i class="fa fa-facebook"></i></a></li>
                                                          <li><a href="http://twitter.com/share?text=Read%20this%20interesting%20blog:&url=http://inomotech.net/blogs/{{ $post->post_id }}" class="tw-button btn btn-primary"><i class="fa fa-twitter"></i></a></li>
                                                          <li><a href="https://plus.google.com/share?url=http://inomotech.net/blogs/{{ $post->post_id }}" class="gp-button btn btn-primary"><i class="fa fa-google-plus"></i></a></li>

                                                      </ul>
                                                  </div><!-- end post-sharing -->

                                                <p>{!! html_entity_decode(substr($post->post_content,0,230).'...') !!}</p>
                                                  <a href="{{ url('blogs/'.$post->post_id) }}" title="" class="readmore">Read full post </a>
                                              </div><!-- end post-padding -->
                                          </div><!-- end content -->
                                      @endforeach
                              </div>
                              </div><!-- end col -->
                          </div><!-- end row -->
                      </div><!-- end container -->
           </section><!-- end section -->

        <section class="section searchbanner1 lightversion">
            <div class="container">
                <div class="section-title text-center">
                    <h3>Company <span>Statistics</span></h3>
                </div><!-- end section-title -->
                <div class="row services-list hover-services text-center">
                    <div class="col-md-3 col-sm-6 wow fadeIn">
                        <div class="box">
                            <i class="fa fa-thumbs-up"></i>
                            <h3>Project Completed</h3>
                            <p class="stat-count">250</p>
                        </div><!-- end box -->
                    </div>

                    <div class="col-md-3 col-sm-6 wow fadeIn">
                        <div class="box">
                            <i class="fa fa-smile-o"></i>
                            <h3>Happy Clients</h3>
                            <p class="stat-count">350</p>
                        </div><!-- end box -->
                    </div>

                    <div class="col-md-3 col-sm-6 wow fadeIn">
                        <div class="box">
                            <i class="fa fa-laptop"></i>
                            <h3>Web Apps</h3>
                            <p class="stat-count">45</p>
                        </div><!-- end box -->
                    </div>

                    <div class="col-md-3 col-sm-6 wow fadeIn">
                        <div class="box">
                            <i class="fa fa-mobile"></i>
                            <h3>Mobile Apps</h3>
                            <p class="stat-count">10</p>
                        </div><!-- end box -->
                    </div><!-- end col -->
                </div>
            </div><!-- end container -->
        </section><!-- end section -->

        <section class="section">
            <div class="container"> 
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="small-title">
                            <h3>Why Choose Us?</h3>
                            <hr>
                        </div><!-- end big-title -->

                        <div class="skills custommargin text-left">
                            <div class="blue-color">
                                <h3>Development</h3>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary  progress-bar-striped active" role="progressbar" aria-valuenow="99" aria-valuemin="0" aria-valuemax="100" style="width: 99%">
                                        <span class="sr-only">99%</span>
                                    </div>
                                </div>
                            </div><!-- end green-color -->

                            <div class="blue-color">
                                <h3>Design</h3>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary  progress-bar-striped active" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 90%">
                                        <span class="sr-only">90%</span>
                                    </div>
                                </div>
                            </div><!-- end green-color -->

                            <div class="blue-color">
                                <h3>Software Quality Assurance Testing</h3>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary  progress-bar-striped active" role="progressbar" aria-valuenow="96" aria-valuemin="0" aria-valuemax="100" style="width: 95%">
                                        <span class="sr-only">95%</span>
                                    </div>
                                </div>
                            </div><!-- end green-color -->

                            <div class="blue-color">
                                <h3>Consultancy</h3>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary  progress-bar-striped active" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 90%">
                                        <span class="sr-only">90%</span>
                                    </div>
                                </div>
                            </div><!-- end green-color -->

                            <div class="blue-color">
                                <h3>Security</h3>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary  progress-bar-striped active" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 95%">
                                        <span class="sr-only">95%</span>
                                    </div>
                                </div>
                            </div><!-- end green-color -->
                        </div><!-- end skills -->
                    </div><!-- end col -->

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="small-title">
                            <h3>Services</h3>
                            <hr>
                        </div><!-- end big-title -->

                        <br>

                        <div class="panel-group first-accordion withicon" id="accordion1">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion1" href="#collapse11"><i class="fa fa-question"></i> Training</a>
                                    </h4>
                                </div>
                                <div id="collapse11" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>Organizing training for users and other consultants.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion1" href="#collapse22"><i class="fa fa-question"></i>Progress Report</a>
                                    </h4>
                                </div>
                                <div id="collapse22" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>Preparing documentation and presenting progress reports to customers.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion1" href="#collapse33"><i class="fa fa-question"></i>Analysing IT requirements</a>
                                    </h4>
                                </div>
                                <div id="collapse33" class="panel-collapse collapse">
                                    <div class="panel-body">
                                      <p>Analyzing IT requirements within companies and giving independent and objective advice on the use of IT.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion1" href="#collapse44"><i class="fa fa-question"></i> Planning Timescales
                                    </h4>
                                </div>
                                <div id="collapse44" class="panel-collapse collapse">
                                    <div class="panel-body">
                                       <p>Clarifying client’s system specifications, understanding their work practices and the nature of their business. Planning timescales and the resources needed.</p>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->

        <section class="smallsec">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 text-center">
                        <h3>Contact us and let's do business together</h3>
                    </div>
                    <div class="col-md-4 text-center">
                        <a href="{{ url(('contact')) }}" class="btn btn-primary btn-lg"><i class="fa fa-comments-o"></i> Contact Us</a>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->



       <section class="section nopadding clearfix">
              <div class="container">
                  <div class="row">
                      <div class="col-md-5 col-sm-12 customsection hidden-sm hidden-xs">
                          <img src="{{ asset('main/upload/section_02.jpg')}}" alt="images">
                          <div class="text_container">
                              <span class="logo"></span>
                              <h4>What We<br>Offer?</h4>
                              <p>Our greatest concern is to provide the best services to people all over Africa and the world at large, & Providing them with quality solutions that will increase efficiency and foster growth and development</p>
                              <a href="{{ url('contact')}}" class="btn btn-primary">Contact Us</a>
                          </div>
                      </div>

                      <div class="col-md-7 col-sm-12">
                          <div class="box-feature-full clearfix">
                              <div class="vertical-elements">
                                  <div class="box">
                                      <div class="icon-container alignleft">
                                          <img src="{{ asset('main/images/icons/features_box_01.png')}}" alt="" class="img-responsive">
                                      </div>

                                      <div class="feature-desc">
                                          <h4>Web/Mobile Application Development</h4>
                                          <p>We are the best in the Web/Mobile development field. We can be trusted!</p>
                                      </div><!-- end desc -->
                                  </div><!-- end featurebox -->

                                  <hr>

                                  <div class="box">
                                      <div class="icon-container alignleft">
                                          <img src="{{ asset('main/images/icons/features_box_02.png')}}" alt="" class="img-responsive">
                                      </div>

                                      <div class="feature-desc">
                                          <h4>IT Consultancy/Business Automation</h4>
                                          <p>We provide the most effective consultancy in the country.</p>
                                      </div><!-- end desc -->
                                  </div><!-- end featurebox -->

                                  <hr>

                                  <div class="box">
                                      <div class="icon-container alignleft">
                                          <img src="{{ asset('main/images/icons/features_box_03.png')}}" alt="" class="img-responsive">
                                      </div>

                                      <div class="feature-desc">
                                          <h4>Motion Graphics / Software testing</h4>
                                          <p>We are the best software quality assurance testing company in Africa</p>
                                      </div><!-- end desc -->
                                  </div><!-- end featurebox -->
                              </div><!-- end vertical -->
                          </div>
                      </div>
                  </div>
              </div>
       </section><!-- end section -->



        <section class="section searchbanner">
            <div class="container">
                <div class="section-title text-center">
                    <h3><span>20,000+</span> SUBSCRIBERS CAN NOT BE WRONG</h3>
                    <p class="last">
                        Subscribe to our newsletter!
                    </p>
                </div><!-- end section-title -->

                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="widget clearfix text-center">
                                <div class="checkdomain-wrapper">
                                    <div class="form-group">
                                        {{--<label class="sr-only" for="domainnamehere-menu1">Domain name</label>--}}
                                        {{--<input type="text" class="form-control" id="domainnamehere-menu1" placeholder="Enter your email here..">--}}
                                        {{--<button type="submit" class="btn btn-default"><i class="fa fa-envelope-o"></i> Go</button>--}}
                            {!! Form::open(array('url'=>'news', 'class'=>'hero-newsletter-form','method'=>'post','files'=>true)) !!}

                                @if( ($errors->any()))
                                    <ul class="alert alert-danger">
                                        @foreach ($errors->all() as $error)
                                            <li class="error_message"> {{ $error }}</li>
                                        @endforeach
                                    </ul>
                                @endif


                                @if (session()->has('message'))
                                    <div class="alert alert-success">
                                    {{ session('message') }}
                                    </div>
                                @endif
                                     <input type="email" name="email" class="form-control" placeholder="Enter your email here..">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-envelope-o"></i> Go</button>

                            {!! Form::close() !!}
                                    </div>
                                </div><!-- end checkdomain-wrapper -->
                        </div><!-- end widget -->
                    </div>
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->


        <section class="section testibanner">
            <div class="container-fluid">
                <div class="section-title text-center">
                    <h3>Happy <span>Testimonials</span></h3>
                    <p class="last">
                        Let's see what other's say about IMT!<br> 10.000+ customers can not be wrong!
                    </p>
                </div><!-- end section-title -->

                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="row testimonials">
                        @foreach($say_all as $say)
                            <div class="col-md-3 col-sm-6 m30">
                             <blockquote>
                                    <p> {!! html_entity_decode($say->say_content) !!}</p>
                                    <span class="clients-name text-primary">— {{ $say->say_name}}</span>
                                    <img class="img-circle img-thumbnail" src="{{ $say->say_image }}" alt="">
                                </blockquote>
                            </div>
                        @endforeach
                        </div>
                    </div><!--/.col-->  
                </div><!--/.row-->
            </div><!-- end container -->
        </section><!-- end section -->

        <section class="section overflow">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="big-title">
                            <h3>
                            <span>Our Latest Clients</span>
                            </h3>
                        </div><!-- end big-title -->

                        <div class="hfeatures">
                            <p>We work in partnership with our clients, advising them to use information technology in order to meet their business objectives or overcome problems. We help to improve the structure and efficiency of IT systems in various organizations. We provide strategic guidance to clients with regard to technology, IT infrastructures and enabling major business processes through enhancements to IT.</p>

                            <ul class="work-elements">
                                @foreach( $client_1 as $clients_1)
                                <li>
                                    <div class="box GrayScale">
                                        <div class="icon-container">
                                            <a href="{{ url('clients/'.$clients_1->client_id) }}"><img src="{{ $clients_1->client_logo }}" alt="" class="img-responsive"></a>
                                        </div>
                                    </div><!-- end featurebox -->
                                </li><!-- end col -->
                                @endforeach

                            </ul>
                        </div><!-- end hfeatures -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->

            <div class="image-box hidden-sm hidden-xs wow slideInRight">
                <img alt="" src="{{ asset('main/upload/device_07.png')}}">
            </div>
        </section><!-- end section -->

        <section class="smallsec">
             <div class="container">
                            <div class="row">
                                <div class="col-md-8 text-center">
                                    <h3>Contact us and let's do business together</h3>
                                </div>
                                <div class="col-md-4 text-center">
                                    <a href="{{ url(('contact')) }}" class="btn btn-primary btn-lg"><i class="fa fa-comments-o"></i> Contact Us</a>
                                </div><!-- end col -->
                            </div><!-- end row -->
                        </div><!-- end container -->
        </section><!-- end section -->

        <footer class="footer lb">
             @include('partials/footer')
        </footer><!-- end footer -->

               @include('partials/below-footer')
    <div class="dmtop">Scroll to Top</div>
</div><!-- end wrapper -->

    <!-- Main Scripts-->
    @include('partials/jsfiles')

</body>

</html>