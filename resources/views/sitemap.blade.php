<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

    {{--Static Pages--}}
    <url>
        <loc>{{ Request::root() }}</loc>
        <priority>1.0</priority>
        <changefreq>daily</changefreq>
    </url>
        <url>
            <loc>{{ Request::root() }}</loc>
        </url>
    <url>
        <loc>{{ Request::root().'/index' }}</loc>
    </url>
    <url>
        <loc>{{ Request::root().'/about' }}</loc>
    </url>
    <url>
        <loc>{{ Request::root().'/blogs' }}</loc>
    </url>
    <url>
        <loc>{{ Request::root().'/contact' }}</loc>
    </url>
      <url>
            <loc>{{ Request::root().'/clients' }}</loc>
      </url>

      <url>
            <loc>{{ Request::root().'/services' }}</loc>
      </url>
     <url>
        <loc>{{ Request::root().'/search' }}</loc>
    </url>



    {{--Dynamic Pages--}}
    @foreach($pages as $post)
        <url>
              <loc>{{ Request::root() . '/page/'.$post->policy_id}}</loc>
        </url>
    @endforeach

     @foreach($posts as $post)
            <url>
              <loc>{{ Request::root() . '/blogs/'.$post->post_id}}</loc>
        </url>
    @endforeach



</urlset>