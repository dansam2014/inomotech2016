<!DOCTYPE html>
<html class="no-js before-run" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap admin template">
  <meta name="author" content="">

  <title>Dashboard - Inomotech</title>

  <link rel="apple-touch-icon" href="{{ asset('assets/images/apple-touch-icon.png') }}">
  <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}">

  <!-- Stylesheets -->
  <link rel="stylesheet" href="{{ asset('admin/assets/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('admin/assets/css/bootstrap-extend.min.css') }}">
  <link rel="stylesheet" href="{{ asset('admin/assets/css/site.min.css') }}">

  <link rel="stylesheet" href="{{ asset('admin/assets/vendor/animsition/animsition.css') }}">
  <link rel="stylesheet" href="{{ asset('admin/assets/vendor/asscrollable/asScrollable.css') }}">
  <link rel="stylesheet" href="{{ asset('admin/assets/vendor/switchery/switchery.css') }}">
  <link rel="stylesheet" href="{{ asset('admin/assets/vendor/intro-js/introjs.css') }}">
  <link rel="stylesheet" href="{{ asset('admin/assets/vendor/slidepanel/slidePanel.css') }}">
  <link rel="stylesheet" href="{{ asset('admin/assets/vendor/flag-icon-css/flag-icon.css') }}">

  <!-- Plugin -->
  <link rel="stylesheet" href="{{ asset('admin/assets/vendor/chartist-js/chartist.css') }}">
  <link rel="stylesheet" href="{{ asset('admin/assets/vendor/jvectormap/jquery-jvectormap.css') }}">

  <!-- Page -->
  <link rel="stylesheet" href="{{ asset('admin/assets/css/../fonts/weather-icons/weather-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('admin/assets/css/dashboard/v1.css') }}">

  <!-- Fonts -->
  <link rel="stylesheet" href="{{ asset('admin/assets/fonts/font-awesome/font-awesome.css') }}">
  <link rel="stylesheet" href="{{ asset('admin/assets/fonts/web-icons/web-icons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('admin/assets/fonts/brand-icons/brand-icons.min.css') }}">
  <!-- <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'> -->


  <!--[if lt IE 9]>
    <script src="{{ asset('assets/vendor/html5shiv/html5shiv.min.js') }}"></script>
    <![endif]-->

  <!--[if lt IE 10]>
    <script src="{{ asset('assets/vendor/media-match/media.match.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/respond/respond.min.js') }}"></script>
    <![endif]-->

  <!-- Scripts -->
  <script src="{{ asset('admin/assets/vendor/modernizr/modernizr.js') }}"></script>
  <script src="{{ asset('admin/assets/vendor/breakpoints/breakpoints.js') }}"></script>
  <script>
    Breakpoints();
  </script>
  @yield('head')
</head>
<body class="dashboard">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega" role="navigation">

        <div class="navbar-header">
      <button type="button" class="navbar-toggle hamburger hamburger-close navbar-toggle-left hided"
      data-toggle="menubar">
        <span class="sr-only">Toggle navigation</span>
        <span class="hamburger-bar"></span>
      </button>

      <button type="button" class="navbar-toggle collapsed" data-target="#site-navbar-collapse"
      data-toggle="collapse">
        <i class="icon wb-more-horizontal" aria-hidden="true"></i>
      </button>
      <button type="button" class="navbar-toggle collapsed" data-target="#site-navbar-search"
      data-toggle="collapse">
        <span class="sr-only">Toggle Search</span>
        <i class="icon wb-search" aria-hidden="true"></i>
      </button>
      <div class="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu">
        <!-- <img class="navbar-brand-logo" src="../assets/images/logo.png" title="Remark"> -->
        <span class="navbar-brand-text"> Inomotech</span>
      </div>
        </div>

        <div class="navbar-container container-fluid">

            <!-- Navbar Collapse -->
            <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
                {{--<i class="fa fa-inbox" style="float: right; font-size: 2em;">1</i>--}}


                <!-- Navbar Toolbar -->
                <ul class="nav navbar-toolbar">
                  <li class="hidden-float" id="toggleMenubar">
                    <a data-toggle="menubar" href="#" role="button">
                      <i class="icon hamburger hamburger-arrow-left">
                          <span class="sr-only">Toggle menubar</span>
                          <span class="hamburger-bar"></span>
                        </i>
                    </a>


                  </li>

                  <li class="hidden-xs" id="toggleFullscreen">
                    <a class="icon icon-fullscreen" data-toggle="fullscreen" href="#" role="button">
                      <span class="sr-only">Toggle fullscreen</span>
                    </a>
                  </li>
                </ul>
                <!-- End Navbar Toolbar -->

                <!-- End Navbar Toolbar Right -->
                </div>
                <!-- End Navbar Collapse -->

                <!-- Site Navbar Seach -->
            <div class="collapse navbar-search-overlap" id="site-navbar-search">
                <form role="search">
                  <div class="form-group">
                    <div class="input-search">
                      <i class="input-search-icon wb-search" aria-hidden="true"></i>
                      <input type="text" class="form-control" name="site-search" placeholder="Search...">
                      <button type="button" class="input-search-close icon wb-close" data-target="#site-navbar-search"
                      data-toggle="collapse" aria-label="Close"></button>
                    </div>
                  </div>
                </form>

            </div>
            <!-- End Site Navbar Seach -->
        </div>
    </nav>
    <div class="site-menubar">
        <div class="site-menubar-body">
            <div>
                <div>
                    <ul class="site-menu">
                        <li class="site-menu-category">&nbsp;</li>
                        <li class="site-menu-item">
                            <a href="{{ url('/') }}" data-slug="periods">
                                <i class="site-menu-icon fa fa-home" aria-hidden="true"></i>
                                <span class="site-menu-title">Dashboard</span>
                            </a>
                        </li>

                        <li class="site-menu-item dropdown">
                            <a href="{{ url('faq') }}" data-slug="periods">
                                <i class="site-menu-icon ffa fa-question-circle" aria-hidden="true"></i>
                                <span class="site-menu-title">FAQ</span>
                            </a>
                        </li>
                        <li class="site-menu-item dropdown">
                            <a href="{{ url('policy') }}" data-slug="periods">
                                <i class="site-menu-icon ffa fa-question-circle" aria-hidden="true"></i>
                                <span class="site-menu-title">Policy</span>
                            </a>
                        </li>
                        <li class="site-menu-item dropdown">
                            <a href="{{ url('say') }}" data-slug="periods">
                                <i class="site-menu-icon ffa fa-question-circle" aria-hidden="true"></i>
                                <span class="site-menu-title">Testimonials</span>
                            </a>
                        </li>

                         <li class="site-menu-item has-sub">
                              <a href="javascript:void(0)" data-slug="layout">
                                <i class="site-menu-icon ffa fa-envelope-o" aria-hidden="true"></i>
                                <span class="site-menu-title">Posts</span>
                                <span class="site-menu-arrow"></span>
                              </a>
                              <ul class="site-menu-sub">
                                <li class="site-menu-item">
                                  <a class="animsition-link" href="{{ url('posts') }}" data-slug="layout-grids">
                                    <i class="site-menu-icon " aria-hidden="true"></i>
                                    <span class="site-menu-title"> Post</span>
                                  </a>
                                </li>
                                  <li class="site-menu-item">
                                      <a class="animsition-link" href="{{ url('unpublish') }}" data-slug="layout-grids">
                                          <i class="site-menu-icon " aria-hidden="true"></i>
                                          <span class="site-menu-title"> Unpublish</span>
                                      </a>
                                  </li>
                                  <li class="site-menu-item">
                                      <a class="animsition-link" href="{{ url('posts-category') }}" data-slug="layout-grids">
                                          <i class="site-menu-icon " aria-hidden="true"></i>
                                          <span class="site-menu-title"> Category</span>
                                      </a>
                                  </li>
                              </ul>
                        </li>

                        <li class="site-menu-item has-sub">
                            <a href="javascript:void(0)" data-slug="layout">
                                <i class="site-menu-icon fa fa-users" aria-hidden="true"></i>
                                <span class="site-menu-title">Team</span>
                                <span class="site-menu-arrow"></span>
                            </a>
                            <ul class="site-menu-sub">
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="{{ url('team') }}" data-slug="layout-grids">
                                        <i class="site-menu-icon " aria-hidden="true"></i>
                                        <span class="site-menu-title"> Team</span>
                                    </a>
                                </li>
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="{{ url('team-category') }}" data-slug="layout-grids">
                                        <i class="site-menu-icon " aria-hidden="true"></i>
                                        <span class="site-menu-title"> Category</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                         <li class="site-menu-item has-sub">
                            <a href="javascript:void(0)" data-slug="layout">
                                <i class="site-menu-icon fa fa-users" aria-hidden="true"></i>
                                <span class="site-menu-title">Clients</span>
                                <span class="site-menu-arrow"></span>
                            </a>
                            <ul class="site-menu-sub">
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="{{ url('clients') }}" data-slug="layout-grids">
                                        <i class="site-menu-icon " aria-hidden="true"></i>
                                        <span class="site-menu-title"> Client</span>
                                    </a>
                                </li>
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="{{ url('client-category') }}" data-slug="layout-grids">
                                        <i class="site-menu-icon " aria-hidden="true"></i>
                                        <span class="site-menu-title"> Category</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="site-menu-item has-sub">
                            <a href="javascript:void(0)" data-slug="layout">
                                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                <span class="site-menu-title"> Comments</span>
                                <span class="site-menu-arrow"></span>
                            </a>
                            <ul class="site-menu-sub">
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="{{ url('commentsComment') }}" data-slug="layout-grids">
                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                        <span class="site-menu-title"> Comment's Comment</span>
                                    </a>
                                </li>
                            </ul>
                            <ul class="site-menu-sub">
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="{{ url('outbox') }}" data-slug="layout-grids">
                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                        <span class="site-menu-title"> Main Comments</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="site-menu-item has-sub">
                            <a href="javascript:void(0)" data-slug="layout">
                                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                <span class="site-menu-title"> Sermon Summary</span>
                                <span class="site-menu-arrow"></span>
                            </a>
                            <ul class="site-menu-sub">
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="{{ url('sermon-summary') }}" data-slug="layout-grids">
                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                        <span class="site-menu-title"> Sermon</span>
                                    </a>
                                </li>
                            </ul>
                            <ul class="site-menu-sub">
                                 <li class="site-menu-item">
                                     <a class="animsition-link" href="{{ url('summaryComment') }}" data-slug="layout-grids">
                                         <i class="fa fa-envelope" aria-hidden="true"></i>
                                         <span class="site-menu-title"> Comment</span>
                                     </a>
                                 </li>
                             </ul>

                        </li>
                        <li class="site-menu-item has-sub">
                            <a href="javascript:void(0)" data-slug="layout">
                                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                <span class="site-menu-title">Message</span>
                                <span class="site-menu-arrow"></span>
                            </a>
                            <ul class="site-menu-sub">
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="{{ url('messages') }}" data-slug="layout-grids">
                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                        <span class="site-menu-title"> Inbox</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="site-menu-item has-sub">
                            <a href="javascript:void(0)" data-slug="layout">
                                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                <span class="site-menu-title">Gallery</span>
                                <span class="site-menu-arrow"></span>
                            </a>
                            <ul class="site-menu-sub">
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="{{ url('gallery') }}" data-slug="layout-grids">
                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                        <span class="site-menu-title"> Add Images</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="site-menu-item has-sub">
                        <a href="javascript:void(0)" data-slug="layout">
                            <i class="fa fa-search" aria-hidden="true"></i>
                            <span class="site-menu-title">Links</span>
                            <span class="site-menu-arrow"></span>
                        </a>
                        <ul class="site-menu-sub">
                            <li class="site-menu-item">
                                <a class="animsition-link" href="{{ url('search') }}" data-slug="layout-grids">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                    <span class="site-menu-title"> Search Terms</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="site-menu-sub">
                            <li class="site-menu-item">
                                <a class="animsition-link" href="{{ url('subscribers') }}" data-slug="layout-grids">
                                    <i class="fa fa-envelop-o" aria-hidden="true"></i>
                                    <span class="site-menu-title"> Newsletter Subscribers</span>
                                </a>
                            </li>
                        </ul>
                    </li>
</ul>
                  </div>
                </div>
            </div>
        </div>


        <div class="site-menubar-footer">
          <a href="javascript: void(0);" class="fold-show" data-placement="top" data-toggle="tooltip"
          data-original-title="Settings">
            <span class="icon wb-settings" aria-hidden="true"></span>
          </a>
          <a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Lock">
            <span class="icon wb-eye-close" aria-hidden="true"></span>
          </a>
          <a href="{{ url('loggout') }}" data-placement="top" data-toggle="tooltip" data-original-title="Logout">
            <span class="icon wb-power" aria-hidden="true"></span>
          </a>
        </div>
    </div>
    <!-- Page -->
    @yield('content')

    <!-- End Page -->

    <!-- Footer -->
    <footer class="site-footer">
        <span class="site-footer-legal">© 2015 Opoku Samuel Daniels</span>
        <div class="site-footer-right">
            Crafted with <i class="red-600 wb wb-heart"></i> by <a href="http://inomotech.net">Inomotech</a>
        </div>
    </footer>

  <!-- Core  -->
  <script src="{{ asset('admin/assets/vendor/jquery/jquery.js') }}"></script>
  <!-- // <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script> -->
  <script src="{{ asset('admin/assets/vendor/bootstrap/bootstrap.js') }}"></script>
  <script src="{{ asset('admin/assets/vendor/animsition/jquery.animsition.js') }}"></script>
  <script src="{{ asset('admin/assets/vendor/asscroll/jquery-asScroll.js') }}"></script>
  <script src="{{ asset('admin/assets/vendor/mousewheel/jquery.mousewheel.js') }}"></script>
  <script src="{{ asset('admin/assets/vendor/asscrollable/jquery.asScrollable.all.js') }}"></script>
  <script src="{{ asset('admin/assets/vendor/ashoverscroll/jquery-asHoverScroll.js') }}"></script>

  <!-- Plugins -->
  <script src="{{ asset('admin/assets/vendor/switchery/switchery.min.js') }}"></script>
  <script src="{{ asset('admin/assets/vendor/intro-js/intro.js') }}"></script>
  <script src="{{ asset('admin/assets/vendor/screenfull/screenfull.js') }}"></script>
  <script src="{{ asset('admin/assets/vendor/slidepanel/jquery-slidePanel.js') }}"></script>

  <script src="{{ asset('admin/assets/vendor/skycons/skycons.js') }}"></script>
  <script src="{{ asset('admin/assets/vendor/chartist-js/chartist.min.js') }}"></script>
  <script src="{{ asset('admin/assets/vendor/aspieprogress/jquery-asPieProgress.min.js') }}"></script>
  <script src="{{ asset('admin/assets/vendor/jvectormap/jquery-jvectormap.min.js') }}"></script>
  <script src="{{ asset('admin/assets/vendor/jvectormap/maps/jquery-jvectormap-ca-lcc-en.js') }}"></script>
  <script src="{{ asset('admin/assets/vendor/matchheight/jquery.matchHeight-min.js') }}"></script>

  <!-- Scripts -->
  <script src="{{ asset('admin/assets/js/core.js') }}"></script>
  <script src="{{ asset('admin/assets/js/site.js') }}"></script>

  <script src="{{ asset('admin/assets/js/sections/menu.js') }}"></script>
  <script src="{{ asset('admin/assets/js/sections/menubar.js') }}"></script>
  <script src="{{ asset('admin/assets/js/sections/sidebar.js') }}"></script>

  <script src="{{ asset('admin/assets/js/configs/config-colors.js') }}"></script>
  <script src="{{ asset('admin/assets/js/configs/config-tour.js') }}"></script>

  <script src="{{ asset('admin/assets/js/components/asscrollable.js') }}"></script>
  <script src="{{ asset('admin/assets/js/components/animsition.js') }}"></script>
  <script src="{{ asset('admin/assets/js/components/slidepanel.js') }}"></script>
  <script src="{{ asset('admin/assets/js/components/switchery.js') }}"></script>
  <script src="{{ asset('admin/assets/js/components/matchheight.js') }}"></script>
  <script src="{{ asset('admin/assets/js/components/jvectormap.js') }}"></script>
  <script>
    $(document).ready(function($) {
      Site.run();
    });
</script>
  @yield('footer')
</body>

</html>