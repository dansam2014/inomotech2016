<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->


<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
     @include('partials/head')
</head>
<body>

	<!-- PRELOADER -->
        <div id="loader">
			<div class="loader-container">
				<img src="{{ asset('main/images/load.gif')}}" alt="" class="loader-site spinner">
			</div>
		</div>
	<!-- END PRELOADER -->

    <div id="wrapper">
        <div class="topbar">
            @include('partials/top-header')
        </div><!-- end topbar -->

        <header class="header">
            @include('partials/header')<!-- end container -->
        </header><!-- end header -->

        <div class="after-header">
             @include('partials/below-header')
        </div><!-- end after-header -->

        <section class="section paralbackground page-banner" style="background-image:url('/main/upload/page_banner_01.jpg');" data-img-width="2000" data-img-height="400" data-diff="100">
        </section><!-- end section -->

        <div class="section page-title">
            <div class="container clearfix">
                <div class="title-area pull-left">
                    <h2>Blogs <small>Lastest from our blog</small></h2>
                </div><!-- /.pull-right -->
                <div class="pull-right hidden-xs">
                    <div class="bread">
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">Blog</li>
                        </ol>
                    </div><!-- end bread -->
                </div><!-- /.pull-right -->
            </div>
        </div><!-- end page-title -->
     
        <section class="section lb">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-12 clearfix">
                        <div class="grid-layout">
                            @foreach($blogs as $post)
                                  <div class="content grid-blog wbp">
                                      <div class="post-media clearfix entry">
                                          <img src="{{ $post->post_image }}" alt="" class="img-responsive">
                                          <div class="magnifier">
                                              <div class="visible-buttons">
                                                  <span><a data-placement="bottom" data-toggle="tooltip" title="" href="{{ url('blogs/'.$post->post_id) }}"><i class="fa fa-link"></i></a></span>
                                              </div>
                                          </div>
                                      </div><!-- end post-media -->

                                      <div class="post-padding clearfix">
                                          <div class="large-post-meta">
                                              <small>&#124;</small>
                                              {{--<span class="hidden-xs"><a href="#"><i class="fa fa-comments-o"></i> 31</a></span>--}}
                                              {{--<small class="hidden-xs">&#124;</small>--}}
                                              {{--<span class="hidden-xs"><a href="single-review.html"><i class="fa fa-eye"></i> 127</a></span>--}}
                                          </div><!-- end meta -->

                                          <h3 class="entry-title"><a href="{{ url('blogs/'.$post->post_id) }}">{{ $post->post_title}}</a></h3>

                                          <div class="post-sharing">
                                              <ul class="list-inline">
                                                  <li><a href="http://www.facebook.com/sharer.php?u=http://inomotech.net/blogs/{{ $post->post_id }}" class="fb-button btn btn-primary"><i class="fa fa-facebook"></i></a></li>
                                                  <li><a href="http://twitter.com/share?text=Read%20this%20interesting%20blog:&url=http://inomotech.net/blogs/{{ $post->post_id }}" class="tw-button btn btn-primary"><i class="fa fa-twitter"></i></a></li>
                                                  <li><a href="https://plus.google.com/share?url=http://inomotech.net/blogs/{{ $post->post_id }}" class="gp-button btn btn-primary"><i class="fa fa-google-plus"></i></a></li>

                                              </ul>
                                          </div><!-- end post-sharing -->

                                        <p>{!! html_entity_decode(substr($post->post_content,0,230).'...') !!}</p>
                                          <a href="{{ url('blogs/'.$post->post_id) }}" title="" class="readmore">Read full post </a>
                                      </div><!-- end post-padding -->
                                  </div><!-- end content -->
                             @endforeach
                        </div>

                       <div>
                        {!! $blogs->render() !!}
                       </div>


                    </div><!-- end col -->

                    <div class="sidebar col-md-4 col-sm-12">
                       @include('partials/sidebar')
                    </div><!-- end sidebar -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->

        <section class="smallsec">
            <div class="container">
                           <div class="row">
                               <div class="col-md-8 text-center">
                                   <h3>Contact us and let's do business together</h3>
                               </div>
                               <div class="col-md-4 text-center">
                                   <a href="{{ url(('contact')) }}" class="btn btn-primary btn-lg"><i class="fa fa-comments-o"></i> Contact Us</a>
                               </div><!-- end col -->
                           </div><!-- end row -->
                       </div><!-- end container -->
        </section><!-- end section -->

        <footer class="footer lb">
           @include('partials/footer')
        </footer><!-- end footer -->

            @include('partials/below-footer')
    <div class="dmtop">Scroll to Top</div>
</div><!-- end wrapper -->

    <!-- Main Scripts-->
    <script src="{{ asset('main/js/jquery.js')}}"></script>
    <script src="{{ asset('main/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('main/js/plugins.js') }}"></script>

</body>
</html>