@extends('layout.base')
@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/timepicki.css')}}">
@endSection()
   
@section('content')
    <div class="page animsition" ng-app="pureApp" ng-controller="CalendarCtrl">
        <div class="page-header">
            <h1 class="page-title">My Calendar</h1>
            <div class="page-header-actions">
                <ol class="breadcrumb">
                    <li><a href="">Home</a></li>
                    <li><a href="javascript:void(0)">Appointments</a></li>
                    <li class="active">Calendar</li>
                </ol>
            </div>
        </div>
        <div class="page-content">
            <!-- Panel -->
            <div class="panel">
                <div class="panel-body container-fluid">
                    <div class="row row-lg">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Scheduled Date</th>
                                    <th>Scheduled Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($calendars as $appt)
                                    <tr>
                                        <td>{{ $appt->users->fullname }}</td>
                                        <td>{{ $appt->given_date }}</td>
                                        <td>{{ $appt->given_time }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- End Panel -->
        </div>
    </div>


    @endSection()
    @section('footer')
        <script src="{{ asset('admin/assets/js/bootstrap-datepicker.js') }}"></script>
        <script src="{{ asset('js/timepicki.js')}}"></script>
        <script>
            $('.datepicker').datepicker();
            $('#startTime').timepicki(); 
            $('#endTime').timepicki(); 
        </script>
        
    <script> 
       
    </script>
    @endSection()