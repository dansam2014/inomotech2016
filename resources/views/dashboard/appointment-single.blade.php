@extends('layout.base')
@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/timepicki.css')}}">
    @endSection()
@section('content')
    <div class="page animsition">
        <div class="page-header">
            <h1 class="page-title">Appointment</h1>
            <div class="page-header-actions">
                <ol class="breadcrumb">
                    <li><a href="">Home</a></li>
                    <li><a href="javascript:void(0)">Tables</a></li>
                    <li class="active">Basic</li>
                </ol>
            </div>
        </div>
        <div class="page-content">
            <!-- Panel -->
            <div class="panel">
                <div class="panel-body container-fluid">
                    <div class="row row-lg">
                        <div class="col-md-12">
                            <!-- Example Basic -->
                            <div class="">
                                    @if( ($errors->any()))
                                        <ul class="alert alert-danger">
                                            @foreach ($errors->all() as $error)
                                                <li class="error_message"> {{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif


                                    @if (session()->has('message'))
                                        <div class="alert alert-success">
                                        {{ session('message') }}
                                        </div>
                                    @endif


                                <div class="row">
                                    <div class="col-md-7 ">
                                        <h4>Info</h4>
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <th>Name</th>
                                                    <td>{{ $appointment->users->fullname }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Phone</th>
                                                    <td><a href="" style="text-decoration: none; color: rgb(118, 131, 143);">{{ $appointment->phone }}</a></td>
                                                </tr>
                                                <tr>
                                                    <th>Desired Date</th>
                                                    <td>{{ $appointment->desired_date }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Desired Time</th>
                                                    <td>{{ $appointment->desired_time }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Message</th>
                                                    <td>{{ $appointment->message }}</td>
                                                </tr>
                                            </tbody>
                                        </table>                                       
                                    </div>
                                    <div class="col-md-5">
                                        {!! Form::open(array('url'=>'appointments/'.$appointment->appointment_id.'/schedule')) !!}
                                            <h4>Schedule Period</h4>
                                            <div class="form-group">
                                                <label>Date</label>
                                                <input name="given_date" type="text" class="form-control datepicker" value="{{ $appointment->given_date }}">
                                            </div>                                            
                                            <div class="form-group">
                                                <label>Time</label>
                                                <input name="given_time" type="text" class="form-control" id="time" value="{{ $appointment->given_time }}">
                                            </div>
                                            <button class="btn btn-success">Submit</button>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                                <div>                                    
                                    <a href="{{url('send-message/'.$appointment->id)}}" class="btn btn-info"><i class="fa fa-envelope"></i> </a>
                                    <a href="{{url('view-appointments/'.$appointment->id)}}" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                                </div>
                            </div>
                            <!-- End Example Basic -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Panel -->
        </div>
    </div>


    @endSection()
    @section('footer')
    <script src="{{ asset('admin/assets/js/bootstrap-datepicker.js') }}"></script>
        <script src="{{ asset('js/timepicki.js')}}"></script>
        <script> 
            $('.datepicker').datepicker({
                format:'yyyy-mm-dd'
            });
            $('#time').timepicki();
        </script>
    @endSection()