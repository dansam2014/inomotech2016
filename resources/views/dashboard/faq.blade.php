@extends('layout.base')
@section('head')
    <link rel="stylesheet" type="text/css" href="{{asset('css/timepicki.css')}}">
    @endSection()
    <script>
        function show(){
            document.getElementById("schedule").style.display="block";
        }
    </script>
@section('content')
    <div class="page animsition">
        <div class="page-header">
            <h1 class="page-title">FAQs Available</h1>
            <div class="page-header-actions">
                <ol class="breadcrumb">
                    <li><a href="">Home</a></li>
                    <li><a href="javascript:void(0)">FAQ's</a></li>
            </div>
        </div>
        <div class="page-content">
            <!-- Panel -->
            <div class="panel">
                <div class="panel-body container-fluid">
                    <div class="row row-lg">
                        <div class="col-md-12">
                            <!-- Example Basic -->
                            <div class="">
                                <h4 class="">FAQs</h4>
                                @if( ($errors->any()))
                                    <ul class="alert alert-danger">
                                        @foreach ($errors->all() as $error)
                                            <li class="error_message"> {{ $error }}</li>
                                        @endforeach
                                    </ul>
                                @endif
                                @if (session()->has('message'))
                                    <div class="alert alert-success">
                                    {{ session('message') }}
                                    </div>
                                @endif

                                <div style="margin-bottom:10px;" class="category">
                                    <a class="btn btn-primary"  data-target="#FormModal" data-toggle="modal"><i class="fa fa-plus-circle"></i> Add FAQ</a>


                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>Title</th>
                                            <th>Content</th>
                                            <th>Date</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                            @foreach( $faqs as $faq)
                                                <tr>
                                                    <td><a href="{{ url('faq-single/'.$faq->id) }}"><i class="fa fa-circle"></i></a></td>
                                                    <td><a href="{{ url('faq-single/'.$faq->id) }}" style="text-decoration: none; color: rgb(118, 131, 143);">{{ $faq->faq_title }}</a></td>
                                                    <td><a href="{{ url('faq-single/'.$faq->id) }}" style="text-decoration: none; color: rgb(118, 131, 143);">{{ substr($faq->faq_content, 0, 150 ).'...' }}</a></td>
                                                    <td>{{ $faq->created_at }}</td>
                                                    <td>
                                                        <a href="{{ url('faq-single/'.$faq->id) }}"><i class="fa fa-pencil"></i></a>

                                                        <a href="{{url('faq/'.$faq->id)}}"><i class="fa fa-trash-o"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- End Example Basic -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Panel -->
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="FormModal" aria-hidden="false" aria-labelledby="exampleFormModalLabel" role="dialog" tabindex="-1">
        <div class="modal-dialog">
            {!! Form::open(array('url'=>'faq', 'method'=>'post','class'=>'modal-content','files'=>true)) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="exampleFormModalLabel">Add FAQ</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::text('faq_title','',array('class'=>'form-control', 'placeholder'=>'FAQ Title')) !!}
                    </div>

                    <div class="col-lg-12 form-group">
                        {!! Form::textarea('faq_content','',array('class'=>'form-control','placeholder'=>'FAQ Content','id'=>'editor')) !!}
                    </div>

                    <div class="col-sm-12 pull-right">
                        <button class="btn btn-primary btn-outline">Submit</button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <!-- End Modal -->


    @endSection()
@section('footer')
    <script src="//cdn.ckeditor.com/4.5.3/standard/ckeditor.js"></script>
    <script>
        //$('#editor').wysihtml5();
        CKEDITOR.replace( 'editor' );

    </script>
    @endSection()