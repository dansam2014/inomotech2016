@extends('layout.base')
@section('head')
    @endSection()
    <script>
        function show(){
            document.getElementById("schedule").style.display="block";
        }
    </script>
@section('content')
    <div class="page animsition">
        <div class="page-header">
            <h1 class="page-title"> Reply</h1>
            <div class="page-header-actions">
                <ol class="breadcrumb">
                    <li><a href="">Home</a></li>
                </ol>
            </div>
        </div>
        <div class="page-content">
            <!-- Panel -->
            <div class="panel">
                <div class="panel-body container-fluid">
                    <div class="row row-lg">
                        <div class="col-md-12">

                            <div class="">
                                    @if( ($errors->any()))
                                        <ul class="alert alert-danger">
                                            @foreach ($errors->all() as $error)
                                                <li class="error_message"> {{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif


                                    @if (session()->has('message'))
                                        <div class="alert alert-success">
                                        {{ session('message') }}
                                        </div>
                                    @endif
            <div class="col-md-8">
            {!! Form::open(array('url'=>'reply_store', 'method'=>'post','class'=>'modal-content','files'=>true)) !!}
            </div>
                 <input type="hidden" name="user_id" value="{{ $post->user_id }}">
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::text('subject','',array('class'=>'form-control', 'placeholder'=>'Subject')) !!}
                    </div>

                    <div class="col-lg-12 form-group">
                        {!! Form::textarea('reply','',array('class'=>'form-control','placeholder'=>'Reply Content','id'=>'editor')) !!}
                    </div>

                    <div class="col-sm-12 pull-right">
                        <button class="btn btn-primary btn-outline">Submit</button>
                    </div>
                </div>
            {!! Form::close() !!}
                </div>

                            </div>

                            <!-- End Example Basic -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Panel -->
        </div>
    </div>

    <!-- End Modal -->


    @endSection()
    @section('footer')
        <script src="//cdn.ckeditor.com/4.5.3/standard/ckeditor.js"></script>
    <script>
        //$('#editor').wysihtml5();
        CKEDITOR.replace( 'editor' );

    </script>
    @endSection()