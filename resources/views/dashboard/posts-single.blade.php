@extends('layout.base')
@section('head')
    @endSection()
    <script>
        function show(){
            document.getElementById("schedule").style.display="block";
        }
    </script>
@section('content')
    <div class="page animsition">
        <div class="page-header">
            <h1 class="page-title">Posts Available</h1>
            <div class="page-header-actions">
                <ol class="breadcrumb">
                    <li><a href="">Home</a></li>
                    <li><a href="javascript:void(0)">Tables</a></li>
                    <li class="active">Basic</li>
                </ol>
            </div>
        </div>
        <div class="page-content">
            <!-- Panel -->
            <div class="panel">
                <div class="panel-body container-fluid">
                    <div class="row row-lg">
                        <div class="col-md-12">

                            <div class="">
                                    @if( ($errors->any()))
                                        <ul class="alert alert-danger">
                                            @foreach ($errors->all() as $error)
                                                <li class="error_message"> {{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif


                                    @if (session()->has('message'))
                                        <div class="alert alert-success">
                                        {{ session('message') }}
                                        </div>
                                    @endif
            <div class="col-md-8">
            {!! Form::open(array('url'=>'posts-single/'.$post->post_id, 'method'=>'put','files'=>true)) !!}
            </div>
                <div class="row">
                    <div class="col-lg-6 form-group">
                        {!! Form::text('post_title',$post->post_title,array('class'=>'form-control', 'placeholder'=>'Post Title')) !!}
                        {!! Form::hidden('post_id',$post->post_id) !!}
                    </div>

                    <div class="col-lg-6 form-group">

                        {!! Form::select('category',$categories, '',array('class'=>'form-control')) !!}


                    </div>

                    <div class="col-lg-12 form-group">
                        {!! Form::textarea('post_content',$post->post_content,array('class'=>'form-control','placeholder'=>'Post Content','id'=>'editor')) !!}
                    </div>
                    <div class="col-lg-12 form-group">
                        {!! Form::file('post_image') !!}
                    </div>

                    <div class="col-sm-12 pull-right">
                        <button class="btn btn-primary btn-outline">Submit</button>
                    </div>
                </div>
            {!! Form::close() !!}
                </div>

                            </div>

                            <!-- End Example Basic -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Panel -->
        </div>
    </div>

    <!-- End Modal -->


    @endSection()
    @section('footer')
        <script src="//cdn.ckeditor.com/4.5.3/standard/ckeditor.js"></script>
    <script>
        //$('#editor').wysihtml5();
        CKEDITOR.replace( 'editor' );

    </script>
    @endSection()