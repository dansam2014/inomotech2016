@extends('layout.base')
@section('head')
    <link rel="stylesheet" type="text/css" href="{{asset('css/timepicki.css')}}">
    @endSection()
    <script>
        function show(){
            document.getElementById("schedule").style.display="block";
        }
    </script>
@section('content')
    <div class="page animsition">
        <div class="page-header">
            <h1 class="page-title">Positions Available</h1>
            <div class="page-header-actions">
                <ol class="breadcrumb">
                    <li><a href="">Home</a></li>
                    <li><a href="javascript:void(0)">Team Category</a></li>
                </ol>
            </div>
        </div>
        <div class="page-content">
            <!-- Panel -->
            <div class="panel">
                <div class="panel-body container-fluid">
                    <div class="row row-lg">
                        <div class="col-md-12">
                            <!-- Example Basic -->
                            <div class="">
                                    @if( ($errors->any()))
                                        <ul class="alert alert-danger">
                                            @foreach ($errors->all() as $error)
                                                <li class="error_message"> {{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif


                                @if (session()->has('message'))
                                    <div class="alert alert-success">
                                    {{ session('message') }}
                                    </div>
                                @endif

                                <div style="margin-bottom:10px;">
                                    <a class="btn btn-primary"  data-target="#FormModal1" data-toggle="modal" style="margin-left: 30px;"><i class="fa fa-plus-circle"></i> Category</a>
                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Position</th>
                                            <th>Date Added</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                            @foreach( $positions  as $position)
                                                <tr>

                                                    <td><a href="" style="text-decoration: none; color: rgb(118, 131, 143);">{{ $position->id }}</a></td>
                                                    <td><a href="" style="text-decoration: none; color: rgb(118, 131, 143);">{{ $position->position }}</a></td>
                                                    <td>{{ $position->created_at }}</td>
                                                    <td>
                                                        <a href="#"><i class="fa fa-pencil"></i></a>

                                                        <a href="{{url('team-category/'.$position->id)}}"><i class="fa fa-trash-o"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- End Example Basic -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Panel -->
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="FormModal1" aria-hidden="false" aria-labelledby="exampleFormModalLabel" role="dialog" tabindex="-1">
        <div class="modal-dialog">
            {!! Form::open(array('url'=>'team-category', 'method'=>'post','class'=>'modal-content')) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="exampleFormModalLabel">Team Position</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-6 form-group">
                        {!! Form::text('position','',array('class'=>'form-control', 'placeholder'=>'Add Position')) !!}
                    </div>
                    <div class="col-sm-12 pull-right">
                        <button class="btn btn-primary btn-outline">Submit</button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <!-- End Modal -->

    @endSection()
@section('footer')
    <script src="{{ asset('js/timepicki.js')}}"></script>
    <script>
        $('#time1').timepicki();
        $('#time2').timepicki();
    </script>
    @endSection()