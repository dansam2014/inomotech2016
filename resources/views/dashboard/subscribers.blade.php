@extends('layout.base')
@section('head')
    <link rel="stylesheet" type="text/css" href="{{asset('css/timepicki.css')}}">
    @endSection()
    <script>
        function show(){
            document.getElementById("schedule").style.display="block";
        }
    </script>
@section('content')
    <div class="page animsition">
        <div class="page-header">
            <h1 class="page-title"> Available Subscribers</h1>
            <div class="page-header-actions">
                <ol class="breadcrumb">
                    <li><a href="">Home</a></li>
                    <li><a href="javascript:void(0)">Subscribers</a></li>
                </ol>
            </div>
        </div>
        <div class="page-content">
            <!-- Panel -->
            <div class="panel">
                <div class="panel-body container-fluid">
                    <div class="row row-lg">
                        <div class="col-md-12">
                            <!-- Example Basic -->
                            <div class="">
                                    @if( ($errors->any()))
                                        <ul class="alert alert-danger">
                                            @foreach ($errors->all() as $error)
                                                <li class="error_message"> {{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif


                                    @if (session()->has('message'))
                                        <div class="alert alert-success">
                                        {{ session('message') }}
                                        </div>
                                    @endif

                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Subscribers</th>
                                            <th> Date </th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                            @foreach( $terms  as $team)
                                                <tr>

                                                    <td><a href="#" style="text-decoration: none; color: rgb(118, 131, 143);">{{ $team->email }}</a></td>
                                                    <td>{{ $team->created_at }}</td>
                                                    <td>

                                                     <a href="#"><i class="fa fa-trash-o" style="cursor: pointer;"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- End Example Basic -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Panel -->
        </div>
    </div>


    @endSection()
@section('footer')
    <script src="//cdn.ckeditor.com/4.5.3/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'editor' );
    </script>
    @endSection()