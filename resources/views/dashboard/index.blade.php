@extends('layout.base')
    @section('content')
        <div class="page">
            <div class="page-content padding-30 container-fluid">
                <div class="row" data-plugin="matchHeight" data-by-row="true">
                    <div class="col-lg-12 col-md-12">
                        <div class="row">
                            <div class="col-sm-3">
                              <!-- Widget -->
                              <div class="widget" onclick="window.location='messages'" style="cursor: pointer">
                                <div class="widget-content padding-30 bg-blue-600">
                                  <div class="widget-watermark darker font-size-60 margin-15"><i class="icon wb-clipboard" aria-hidden="true"></i></div>
                                  <div class="counter counter-md counter-inverse text-left">
                                    <div class="counter-number-group">
                                      <span class="counter-number">{{ count($messages) }}</span>
                                      <span class="counter-number-related text-capitalize">Message(s)</span>
                                    </div>
                                    <div class="counter-label text-capitalize"><a href="" style="color: #FFF;">View</a></div>
                                  </div>
                                </div>
                              </div>
                              <!-- End Widget -->
                            </div>
                            <div class="col-sm-3">
                              <!-- Widget -->
                              <div class="widget" onclick="window.location='posts'" style="cursor: pointer">
                                <div class="widget-content padding-30 bg-green-600">
                                  <div class="widget-watermark darker font-size-60 margin-15"><i class="icon wb-clipboard" aria-hidden="true"></i></div>
                                  <div class="counter counter-md counter-inverse text-left">
                                    <div class="counter-number-group">
                                      <span class="counter-number">{{ count($posts_) }}</span>
                                      <span class="counter-number-related text-capitalize">Blog(s)</span>
                                    </div>
                                    <div class="counter-label text-capitalize" >View</div>
                                  </div>
                                </div>
                              </div>
                              <!-- End Widget -->
                            </div>
                            <div class="col-sm-3">
                              <!-- Widget -->
                              <div class="widget" onclick="window.location='search'" style="cursor: pointer">
                                <div class="widget-content padding-30 bg-red-600">
                                  <div class="widget-watermark darker font-size-60 margin-15"><i class="icon wb-clipboard" aria-hidden="true"></i></div>
                                  <div class="counter counter-md counter-inverse text-left">
                                    <div class="counter-number-group">
                                      <span class="counter-number">{{ count($search_) }}</span>
                                      <span class="counter-number-related text-capitalize">Search</span>
                                    </div>
                                    <div class="counter-label text-capitalize">View</div>
                                  </div>
                                </div>
                              </div>
                              <!-- End Widget -->
                            </div>
                            <div class="col-sm-3">
                              <!-- Widget -->
                              <div class="widget" onclick="window.location='clients'" style="cursor: pointer">
                                <div class="widget-content padding-30 bg-yellow-600">
                                  <div class="widget-watermark darker font-size-60 margin-15"><i class="icon wb-clipboard" aria-hidden="true"></i></div>
                                  <div class="counter counter-md counter-inverse text-left">
                                    <div class="counter-number-group">
                                      <span class="counter-number">{{ count($clients_) }}</span>
                                      <span class="counter-number-related text-capitalize">Clients</span>
                                    </div>
                                    <div class="counter-label text-capitalize">View</div>
                                  </div>
                                </div>
                              </div>
                              <!-- End Widget -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endSection()