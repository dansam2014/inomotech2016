@extends('layout.base')
@section('head')
    <link rel="stylesheet" type="text/css" href="{{asset('css/timepicki.css')}}">
    @endSection()
    <script>
        function show(){
            document.getElementById("schedule").style.display="block";
        }
    </script>
@section('content')
    <div class="page animsition">
        <div class="page-header">
            <h1 class="page-title">Posts Available</h1>
            <div class="page-header-actions">
                <ol class="breadcrumb">
                    <li><a href="">Home</a></li>
                    <li><a href="javascript:void(0)">Unpublished</a></li>
                </ol>
            </div>
        </div>
        <div class="page-content">
            <!-- Panel -->
            <div class="panel">
                <div class="panel-body container-fluid">
                    <div class="row row-lg">
                        <div class="col-md-12">
                            <!-- Example Basic -->
                            {!! Form::open(array('url'=>'unpublish-posts', 'method'=>'post','role'=>'search','id'=>'update')) !!}
                            <div class="">
                                <h4 class="">Unpublished Posts</h4>
                                    @if( ($errors->any()))
                                        <ul class="alert alert-danger">
                                            @foreach ($errors->all() as $error)
                                                <li class="error_message"> {{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif


                                    @if (session()->has('message'))
                                        <div class="alert alert-success">
                                        {{ session('message') }}
                                        </div>
                                    @endif


                                <div class="table-responsive">

                                    <div style="float:right;">
                                        @if(count($posts ))
                                            <div class="form-group">
                                                <select class="form-control" id="sel1" name="action1" style="width: 200px;" onchange="submit()">
                                                    <option value="">Update</option>
                                                    <option value="publish">Publish</option>
                                                    <option value="delete">Delete</option>
                                                </select>
                                            </div>
                                            <noscript>
                                                <button type="submit" class="btn btn-default">Submit</button>
                                            </noscript>
                                        @endif
                                    </div>

                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>Title</th>
                                            <th>Content</th>
                                            <th>Date</th>
                                            <th>Scheduled Date </th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                            @foreach( $posts  as $post)
                                                <tr>

                                                    <td><input type="checkbox" name="post1[]" value="{{$post->post_id}}"></td>
                                                    <td><a href="{{ url('posts-single/'.$post->post_id) }}" style="text-decoration: none; color: rgb(118, 131, 143);">{{ $post->post_title }}</a></td>
                                                    <td><a href="{{ url('posts-single/'.$post->post_id) }}" style="text-decoration: none; color: rgb(118, 131, 143);">{{ substr($post->post_content, 0, 150 ).'...' }}</a></td>
                                                    <td>{{ $post->created_at }}</td>
                                                    <td>{{ $post->schedule_date }}</td>
                                                    <td>
                                                </tr>
                                            @endforeach


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {!! Form::close() !!}
                            <!-- End Example Basic -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Panel -->
        </div>
    </div>



    @endSection()
@section('footer')
    <script src="{{ asset('js/timepicki.js')}}"></script>
    <script>
        $('#time1').timepicki();
        $('#time2').timepicki();
    </script>
    @endSection()