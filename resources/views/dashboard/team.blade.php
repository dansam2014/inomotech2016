@extends('layout.base')
@section('head')
    <link rel="stylesheet" type="text/css" href="{{asset('css/timepicki.css')}}">
    @endSection()
    <script>
        function show(){
            document.getElementById("schedule").style.display="block";
        }
    </script>
@section('content')
    <div class="page animsition">
        <div class="page-header">
            <h1 class="page-title">Posts Available</h1>
            <div class="page-header-actions">
                <ol class="breadcrumb">
                    <li><a href="">Home</a></li>
                    <li><a href="javascript:void(0)">Teams</a></li>
                </ol>
            </div>
        </div>
        <div class="page-content">
            <!-- Panel -->
            <div class="panel">
                <div class="panel-body container-fluid">
                    <div class="row row-lg">
                        <div class="col-md-12">
                            <!-- Example Basic -->
                            <div class="">
                                <h4 class="">Posts</h4>
                                    @if( ($errors->any()))
                                        <ul class="alert alert-danger">
                                            @foreach ($errors->all() as $error)
                                                <li class="error_message"> {{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif


                                    @if (session()->has('message'))
                                        <div class="alert alert-success">
                                        {{ session('message') }}
                                        </div>
                                    @endif

                                <div style="margin-bottom:10px;" class="category">
                                    <a class="btn btn-primary"  data-target="#FormModal" data-toggle="modal"><i class="fa fa-plus-circle"></i> Add Position</a>

                                    <a class="btn btn-primary"  data-target="#FormModal1" data-toggle="modal" style="margin-left: 30px;"><i class="fa fa-plus-circle"></i> Category</a>
                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Position</th>
                                            <th>Biography</th>
                                            <th> Date </th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                            @foreach( $teams  as $team)
                                                <tr>

                                                    <td><a href="{{ url('team/'.$team->team_id) }}" style="text-decoration: none; color: rgb(118, 131, 143);">{{ $team->fullname }}</a></td>
                                                    <td><a href="{{ url('team/'.$team->team_id) }}" style="text-decoration: none; color: rgb(118, 131, 143);">{{ $team->position }}</a></td>
                                                    <td><a href="{{ url('team/'.$team->team_id) }}" style="text-decoration: none; color: rgb(118, 131, 143);">{{ substr($team->biography, 0, 150 ).'...' }}</a></td>
                                                    <td>{{ $team->created_at }}</td>
                                                    <td>
                                                        <a href="{{ url('team/'.$team->team_id) }}" style="margin-right: 20px;"><i class="fa fa-pencil"></i></a>

                                                     <a href="{{url('teamDestroy/'.$team->team_id)}}"><i class="fa fa-trash-o" style="cursor: pointer;"></i></a>
                                                    </td>
                                                </tr>
                                               {{-- <script>
                                                    function deleteteamFunction() {
                                                        var r = confirm("Do you want to delete this team member?");
                                                        if (r == true) {
                                                            window.location="{{url('team1/'.$team->id)}}";
                                                        } else {

                                                        }
                                                    }
                                                </script>--}}
                                            @endforeach


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- End Example Basic -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Panel -->
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="FormModal" aria-hidden="false" aria-labelledby="exampleFormModalLabel" role="dialog" tabindex="-1">
        <div class="modal-dialog">
            {!! Form::open(array('url'=>'team', 'method'=>'post','class'=>'modal-content','files'=>true)) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="exampleFormModalLabel">Add Team</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-6 form-group">
                        {!! Form::text('fullname','',array('class'=>'form-control', 'placeholder'=>'Fullname')) !!}
                    </div>

                    <div class="col-lg-6 form-group">
                        {!! Form::select('position',$position, '',array('class'=>'form-control')) !!}
                    </div>

                    <div class="col-lg-6 form-group">

                      {{--  {!! Form::select('category',$categories, '',array('class'=>'form-control')) !!}--}}

                     </div>

                    <div class="col-lg-12 form-group">
                        {!! Form::textarea('biography','',array('class'=>'form-control','placeholder'=>'Biography','id'=>'editor')) !!}
                    </div>
                    <div class="col-lg-12 form-group">
                        {!! Form::file('team_image') !!}
                    </div>

                    <div class="col-lg-12 form-group">
                        {!! Form::text('facebook','',array('class'=>'form-control', 'placeholder'=>'Facebook')) !!}
                    </div>
                    <div class="col-lg-12 form-group">
                        {!! Form::text('twitter','',array('class'=>'form-control', 'placeholder'=>'Twitter')) !!}
                    </div>

                    <div class="col-sm-12 pull-right">
                        <button class="btn btn-primary btn-outline">Submit</button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <!-- End Modal -->

    <!-- Modal -->
    <div class="modal fade" id="FormModal1" aria-hidden="false" aria-labelledby="exampleFormModalLabel" role="dialog" tabindex="-1">
        <div class="modal-dialog">
            {!! Form::open(array('url'=>'team-category', 'method'=>'post','class'=>'modal-content')) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="exampleFormModalLabel">Team Position</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-6 form-group">
                        {!! Form::text('position','',array('class'=>'form-control', 'placeholder'=>'Add Category')) !!}
                    </div>
                    <div class="col-sm-12 pull-right">
                        <button class="btn btn-primary btn-outline">Submit</button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <!-- End Modal -->

    @endSection()
@section('footer')
    <script src="//cdn.ckeditor.com/4.5.3/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'editor' );
    </script>
    @endSection()