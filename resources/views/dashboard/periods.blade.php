@extends('layout.base')
@section('head')
     <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/timepicki.css')}}">
@endSection()
@section('content')
<div class="page animsition">
    <div class="page-header">
        <h1 class="page-title">Periods Available</h1>
        <div class="page-header-actions">
            <ol class="breadcrumb">
              <li><a href="{{ url('/') }}">Home</a></li>
              <li><a href="javascript:void(0)">Periods</a></li>
            </ol>
        </div>
    </div>
    <div class="page-content">
            <!-- Panel -->
            <div class="panel">
                <div class="panel-body container-fluid">
                    <div class="row row-lg">
                        <div class="col-md-12">
                            <!-- Example Basic -->
                            <div class="">
                                <h4 class="">Period</h4>
                                <div style="margin-bottom:10px;">
                                    <a class="btn btn-primary"  data-target="#FormModal" data-toggle="modal"><i class="fa fa-plus-circle"></i> Add Period</a>
                                </div>
                                @if (session()->has('message'))
                                    <div class="alert alert-success">
                                        {{ session('message') }}
                                    </div>
                                @endif
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Date</th>
                                                <th>Period</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($periods as $k => $period)
                                            <tr>
                                                <td>{{++$k}}</td>
                                                <td>{{ $period->day }}</td>
                                                <td>{{ $period->start_time }} - {{ $period->end_time }}</td>
                                                <td>
                                                    <a class="btn btn-danger btn-sm" href="{{url('periods/'.$period->id)}}"><i class="fa fa-trash-o"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                  </table>
                                </div>
                            </div>
                            <!-- End Example Basic -->
                        </div>           
                    </div>
                </div>
            </div>
            <!-- End Panel -->
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="FormModal" aria-hidden="false" aria-labelledby="exampleFormModalLabel" role="dialog" tabindex="-1">
        <div class="modal-dialog">
            {!! Form::open(array('url'=>'periods', 'method'=>'post','class'=>'modal-content')) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="exampleFormModalLabel">Add Period</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 form-group">
                            <div class="input-group">
                                <input type="text" name="day" class="form-control datepicker" placeholder='Select Date'>
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-6 form-group">
                            {!! Form::text('start_time','',array('class'=>'form-control','id'=>'time1', 'placeholder'=>'Start Time')) !!}
                        </div>
                        <div class="col-lg-6 form-group">
                            {!! Form::text('end_time','',array('class'=>'form-control','id'=>'time2', 'placeholder'=>'End Time')) !!}
                        </div>                        
                        <div class="col-sm-12 pull-right">
                            <button class="btn btn-primary btn-outline">Submit</button>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
    <!-- End Modal -->
   
@endSection()
@section('footer')
    <script src="{{ asset('admin/assets/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('js/timepicki.js')}}"></script>
    <script> 
        $('.datepicker').datepicker({
            format:'yyyy-mm-dd'
        });
        $('#time1').timepicki(); 
        $('#time2').timepicki(); 
    </script>
@endSection()