@extends('layout.base')
@section('head')
    @endSection()

@section('content')
    <div class="page animsition">
        <div class="page-header">
            <h1 class="page-title">Send Reply</h1>
            <div class="page-header-actions">
                <ol class="breadcrumb">
                    <li><a href="">Home</a></li>
                    <li><a href="javascript:void(0)">Tables</a></li>
                    <li class="active">Basic</li>
                </ol>
            </div>
        </div>
        <div class="page-content">
            <!-- Panel -->
            <div class="panel">
                <div class="panel-body container-fluid">
                    <div class="row row-lg">
                        <div class="col-md-12">

                            <div class="">
                                    @if( ($errors->any()))
                                        <ul class="alert alert-danger">
                                            @foreach ($errors->all() as $error)
                                                <li class="error_message"> {{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif


                                    @if (session()->has('message'))
                                        <div class="alert alert-success">
                                        {{ session('message') }}
                                        </div>
                                    @endif


            @if(count($user->user_id))
             <div class="col-md-8">
                {!! Form::open(array('url'=>'message', 'method'=>'post','class'=>'modal-content','files'=>true)) !!}

                </div>
                    <div class="row">
                        <div class="col-lg-12 form-group">
                            {!! Form::text('title','',array('class'=>'form-control', 'placeholder'=>'Message Title')) !!}
                            {!! Form::hidden('user_id',$user->user_id) !!}
                        </div>

                        <div class="col-lg-12 form-group">
                            {!! Form::textarea('reply','',array('class'=>'form-control','placeholder'=>'Message Content','id'=>'editor')) !!}
                        </div>
                        <div class="col-sm-12 pull-right">
                            <button class="btn btn-primary btn-outline">Submit</button>
                        </div>

                {!! Form::close() !!}
             </div>
            @else
                <div class="col-md-8">
                    {!! Form::open(array('url'=>'message', 'method'=>'post','class'=>'modal-content','files'=>true)) !!}

                </div>
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::text('post_title','',array('class'=>'form-control', 'placeholder'=>'Post Title')) !!}
                    </div>

                    <div class="col-lg-12 form-group">
                        {!! Form::textarea('post_content','',array('class'=>'form-control','placeholder'=>'Post Content','id'=>'editor')) !!}
                    </div>
                    <div class="col-sm-12 pull-right">
                        <button class="btn btn-primary btn-outline">Submit</button>
                    </div>

                    {!! Form::close() !!}
                </div>
            @endif

                </div></div>

                            <!-- End Example Basic -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Panel -->
        </div>
    </div>

    <!-- End Modal -->


    @endSection()
    @section('footer')
        <script src="//cdn.ckeditor.com/4.5.3/standard/ckeditor.js"></script>
    <script>
        //$('#editor').wysihtml5();
        CKEDITOR.replace( 'editor' );

    </script>
    @endSection()