@extends('layout.base')
@section('head')
    <link rel="stylesheet" type="text/css" href="{{asset('css/timepicki.css')}}">
    @endSection()
    <script>
        function show(){
            document.getElementById("schedule").style.display="block";
        }
    </script>
@section('content')
    <div class="page animsition">
        <div class="page-header">
            <h1 class="page-title">FAQs Available</h1>
            <div class="page-header-actions">
                <ol class="breadcrumb">
                    <li><a href="">Home</a></li>
                    <li><a href="javascript:void(0)">Tables</a></li>
                    <li class="active">Basic</li>
                </ol>
            </div>
        </div>
        <div class="page-content">
            <!-- Panel -->
            <div>
                <div >
                    <div class="row row-lg">
                        <div class="col-md-12">
                            <!-- Example Basic -->
                            <div class="">
                                @if( ($errors->any()))
                                    <ul class="alert alert-danger">
                                        @foreach ($errors->all() as $error)
                                            <li class="error_message"> {{ $error }}</li>
                                        @endforeach
                                    </ul>
                                @endif
                                @if (session()->has('message'))
                                    <div class="alert alert-success">
                                    {{ session('message') }}
                                    </div>
                                @endif
                                <div>
                                    {!! Form::open(array('url'=>'faq-single/'.$faq->id, 'method'=>'put','class'=>'modal-content','files'=>true)) !!}
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-lg-12 form-group">
                                                {!! Form::text('faq_title',$faq->faq_title,array('class'=>'form-control')) !!}
                                            </div>

                                            <div class="col-lg-12 form-group">
                                                {!! Form::textarea('faq_content',html_entity_decode($faq->faq_content),array('class'=>'form-control','id'=>'editor')) !!}
                                            </div>

                                            <div class="col-sm-12 pull-right">
                                                <button class="btn btn-primary btn-outline">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            <!-- End Example Basic -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Panel -->
        </div>
    </div>



    @endSection()
@section('footer')
@section('footer')
    <script src="//cdn.ckeditor.com/4.5.3/standard/ckeditor.js"></script>
    <script>
        //$('#editor').wysihtml5();
        CKEDITOR.replace( 'editor' );

    </script>
    @endSection()