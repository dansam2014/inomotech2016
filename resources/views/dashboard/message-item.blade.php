@extends('layout.base')
@section('head')
    <link rel="stylesheet" type="text/css" href="{{asset('css/timepicki.css')}}">
    @endSection()
    <script>
        function show(){
            document.getElementById("schedule").style.display="block";
        }
    </script>
@section('content')
    <div class="page animsition">
        <div class="page-header">
            <h1 class="page-title">Messages</h1>
            <div class="page-header-actions">
                <ol class="breadcrumb">
                    <li><a href="">Home</a></li>
                    <li><a href="javascript:void(0)">Messages</a></li>
            </div>
        </div>
        <div class="page-content">
            <!-- Panel -->
            <div class="panel">
                <div class="panel-body container-fluid">
                    <div class="row row-lg">
                        <div class="col-md-12">
                            <!-- Example Basic -->
                            {!! Form::open(array('url'=>'update', 'method'=>'post','role'=>'search','id'=>'update')) !!}
                            <div class="">
                                    @if( ($errors->any()))
                                        <ul class="alert alert-danger">
                                            @foreach ($errors->all() as $error)
                                                <li class="error_message"> {{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif


                                    @if (session()->has('message'))
                                        <div class="alert alert-success">
                                        {{ session('message') }}
                                        </div>
                                    @endif

                                        <div style="margin-bottom:10px;" class="category">
                                            <a class="btn btn-primary" onclick="window.location='{{ url('message/'.$post->user_id) }}'" ><i class="fa fa-plus-circle"></i> Reply</a>
                                        </div>
                                <div class="table-responsive">

                                    <div style="float:right;">
                                        @if(count($post ))
                                            <div class="form-group">
                                                <select class="form-control" id="sel1" name="action" style="width: 200px;" onchange="submit()">
                                                    <option value="">Update</option>
                                                    <option value="publish">Publish</option>
                                                    <option value="unpublish">Unpublish</option>
                                                    <option value="delete">Delete</option>
                                                </select>
                                            </div>
                                            <noscript>
                                                <button type="submit" class="btn btn-default">Submit</button>
                                            </noscript>
                                        @endif
                                    </div>

                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>Name</th>
                                            <th>Contact</th>
                                            <th>Message</th>
                                            <th>date</th>
                                            <th>Reply</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                                <tr>

                                                    <td><input type="checkbox" name="post[]" value="{{$post->user_id}}"></td>
                                                    <td><a href="" style="text-decoration: none; color: rgb(118, 131, 143);">{{ $post->fullname }}</a></td>
                                                    <td><a href="" style="text-decoration: none; color: rgb(118, 131, 143);">{{ $post->phone }}</a></td>
                                                    <td>{!! html_entity_decode($post->comments) !!}</td>
                                                    <td>{{ $post->created_at }}</td>
                                                    <td><i class="fa fa-envelope-o"></i></td>
                                                    <td>
                                                </tr>


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {!! Form::close() !!}
                            <!-- End Example Basic -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Panel -->
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="FormModal" aria-hidden="false" aria-labelledby="exampleFormModalLabel" role="dialog" tabindex="-1">
        <div class="modal-dialog">
            {!! Form::open(array('url'=>'posts', 'method'=>'post','class'=>'modal-content','files'=>true)) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="exampleFormModalLabel">Add Post</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 form-group" onclick="show()">
                       <div class="btn btn-primary btn-outline">Schedule Post</div>
                    </div>
                    <div class="col-lg-12 form-group" id="schedule" style="display: none">
                        {!! Form::text('schedule_date','',array('class'=>'form-control', 'placeholder'=>'Choose Date')) !!}
                    </div>
                    <div class="col-lg-6 form-group">
                        {!! Form::text('post_title','',array('class'=>'form-control', 'placeholder'=>'Post Title')) !!}
                    </div>

                    <div class="col-lg-12 form-group">
                        {!! Form::textarea('post_content','',array('class'=>'form-control','placeholder'=>'Post Content')) !!}
                    </div>
                    <div class="col-lg-12 form-group">
                        {!! Form::file('post_image') !!}
                    </div>

                    <div class="col-sm-12 pull-right">
                        <button class="btn btn-primary btn-outline">Submit</button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <!-- End Modal -->


    @endSection()
@section('footer')
    <script src="{{ asset('js/timepicki.js')}}"></script>
    <script>
        $('#time1').timepicki();
        $('#time2').timepicki();
    </script>
    @endSection()