@extends('layout.base')
    @section('head')
        <link rel="stylesheet" type="text/css" href="{{asset('css/timepicki.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/bootstrap-datepicker.css') }}">
    @endSection()
    @section('content')
        <div class="page animsition">
            <div class="page-header">
                <h1 class="page-title">Appointments Available</h1>
                <div class="page-header-actions">
                    <ol class="breadcrumb">
                        <li><a href="">Home</a></li>
                        <li><a href="javascript:void(0)">Tables</a></li>
                        <li class="active">Basic</li>
                    </ol>
                </div>
            </div>
            <div class="page-content">
                <!-- Panel -->
                <div class="panel">
                    <div class="panel-body container-fluid">
                        <div class="row row-lg">
                            <div class="col-md-12">
                                <!-- Example Basic -->
                                <div class="">
                                    @if( ($errors->any()))
                                        <ul class="alert alert-danger">
                                            @foreach ($errors->all() as $error)
                                                <li class="error_message"> {{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif


                                    @if (session()->has('message'))
                                        <div class="alert alert-success">
                                        {{ session('message') }}
                                        </div>
                                    @endif


                                    <div class="table-responsive">
                                        {!! Form::open(array('url'=>'appointment-status', 'method'=>'post','class'=>'','role'=>'search')) !!}

                                            <div style="float:right;">
                                                @if(count($appointments ))
                                                    <div class="form-group">
                                                        <select class="form-control" id="sel1" name="action" style="width: 200px;" onchange="submit()">
                                                            <option value="">Update</option>
                                                            <option value="seen">Seen</option>
                                                            <option value="unseen">Unseen</option>
                                                            <option value="delete">Delete</option>
                                                        </select>
                                                    </div>
                                                    <noscript>
                                                        <button type="submit" class="btn btn-default">Submit</button>
                                                    </noscript>

                                                @endif
                                            </div>

                                            @if(count($appointments)>0)
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th>Name</th>
                                                            <th>Phone</th>
                                                            <th>Desired Date</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach( $appointments as $appointment)
                                                            <tr>
                                                                <td><input type="checkbox" name="appointment[]" value="{{$appointment->appointment_id}}"></td>
                                                                <td><a href="{{url('appointments/'.$appointment->id)}}" style="text-decoration: none; color: rgb(118, 131, 143);" >{{ $appointment->users->fullname }}</a></td>
                                                                <td>{{ $appointment->phone }}</td>
                                                                <td>{{ $appointment->desired_date }}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                 </table>
                                            @endif
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endSection()
    @section('footer')
        <script src="{{ asset('admin/assets/js/bootstrap-datepicker.js') }}"></script>
        <script src="{{ asset('js/timepicki.js')}}"></script>
        <script> 
            $('.datepicker').datepicker({
                format:'yyyy-mm-dd'
            });
            $('#time').timepicki();
        </script>
    @endSection()