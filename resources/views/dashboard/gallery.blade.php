@extends('layout.base')
@section('head')
    <link rel="stylesheet" type="text/css" href="{{asset('css/timepicki.css')}}">
    @endSection()
    <script>
        function show(){
            document.getElementById("schedule").style.display="block";
        }
    </script>
@section('content')
    <div class="page animsition">
        <div class="page-header">
            <h1 class="page-title">Galleries Album</h1>
            <div class="page-header-actions">
                <ol class="breadcrumb">
                    <li><a href="">Home</a></li>
                    <li><a href="javascript:void(0)">Teams</a></li>
                </ol>
            </div>
        </div>
        <div class="page-content">
            <!-- Panel -->
            <div class="panel">
                <div class="panel-body container-fluid">
                    <div class="row row-lg">
                        <div class="col-md-12">
                            <!-- Example Basic -->
                            <div class="">
                                <h4 class="">Galleries</h4>
                                    @if( ($errors->any()))
                                        <ul class="alert alert-danger">
                                            @foreach ($errors->all() as $error)
                                                <li class="error_message"> {{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif


                                    @if (session()->has('message'))
                                        <div class="alert alert-success">
                                        {{ session('message') }}
                                        </div>
                                    @endif

                                <div style="margin-bottom:10px;" class="category">
                                    <a class="btn btn-primary"  data-target="#FormModal" data-toggle="modal"><i class="fa fa-plus-circle"></i> Add Images</a>


                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th> Date </th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                            @foreach( $teams  as $team)
                                                <tr>

                                                    <td><a href="#" style="text-decoration: none; color: rgb(118, 131, 143);">{{ $team->gallery_name }}</a></td>
                                                    <td><a href="#" style="text-decoration: none; color: rgb(118, 131, 143);">{{ html_entity_decode(substr($team->gallery_description, 0, 150 ).'...') }}</a></td>

                                                    <td>{{ $team->created_at }}</td>
                                                    <td>
                                                        <a href="{{ url('team/'.$team->gallery_id) }}" style="margin-right: 20px;"><i class="fa fa-pencil"></i></a>

                                                     <a href="{{url('teamDestroy/'.$team->gallery_id)}}"><i class="fa fa-trash-o" style="cursor: pointer;"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- End Example Basic -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Panel -->
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="FormModal" aria-hidden="false" aria-labelledby="exampleFormModalLabel" role="dialog" tabindex="-1">
        <div class="modal-dialog">
            {!! Form::open(array('url'=>'album', 'method'=>'post','class'=>'modal-content','files'=>true)) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="exampleFormModalLabel">Add Team</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::text('album_name','',array('class'=>'form-control', 'placeholder'=>'Album Name')) !!}
                    </div>

                    <div class="col-lg-12 form-group">
                        {!! Form::textarea('album_description','',array('class'=>'form-control','placeholder'=>'Album Description','id'=>'editor')) !!}
                    </div>
                    <div class="col-lg-12 form-group">
                        {!! Form::file('album_image[]',array('multiple'=>true)) !!}
                    </div>

                    <div class="col-sm-12 pull-right">
                        <button class="btn btn-primary btn-outline">Submit</button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <!-- End Modal -->



    @endSection()
    @section('footer')
    <script src="//cdn.ckeditor.com/4.5.3/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'editor' );
    </script>
    @endSection()