@extends('layout.base')
@section('head')
    @endSection()
@section('content')
    <div class="page animsition">
        <div class="page-header">
            <h1 class="page-title">Client Information</h1>
            <div class="page-header-actions">
                <ol class="breadcrumb">
                    <li><a href="">Home</a></li>
                    <li><a href="javascript:void(0)">Client</a></li>
                </ol>
            </div>
        </div>
        <div class="page-content">
    </div>

        <div>
        {!! Form::open(array('url'=>'clientSingle', 'method'=>'POST','class'=>'modal-content','files'=>true)) !!}
    @if( ($errors->any()))
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li class="error_message"> {{ $error }}</li>
            @endforeach
        </ul>
    @endif


    @if (session()->has('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
            <div >

            </div>
            <div style="margin-left: 30px;">
                <div class="row">
                    <div class="col-lg-6 form-group">
                        {!! Form::text('name',$team->name,array('class'=>'form-control')) !!}

                    </div>

                    <div class="col-lg-6 form-group">
                     {!! Form::hidden('client_id',$team->client_id) !!}
                        {!! Form::text('category',$team->category,array('class'=>'form-control')) !!}
                    </div>
                    <div class="col-lg-12 form-group">
                        {!! Form::textarea('description',$team->description,array('class'=>'form-control','id'=>'editor')) !!}
                    </div>

                    <div class="col-lg-6 form-group">
                        {!! Form::text('link',$team->link,array('class'=>'form-control')) !!}
                    </div>
                    <div class="col-lg-6 form-group">
                        {!! Form::text('priority',$team->priority,array('class'=>'form-control')) !!}
                    </div>
                    <div class="col-lg-6 form-group">
                        Client Project: {!! Form::file('client_image') !!}
                    </div>
                     <div class="col-lg-6 form-group">
                        Client Logo:{!! Form::file('client_logo') !!}
                    </div>

                    <div class="col-sm-12 pull-right" >
                        <button class="btn btn-primary btn-outline">Submit</button>
                    </div>

                </div>
            </div>
            {!! Form::close() !!}
        </div>




    @endSection()
@section('footer')
    <script src="//cdn.ckeditor.com/4.5.3/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'editor' );
    </script>
    @endSection()