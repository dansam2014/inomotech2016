@extends('layout.base')
@section('head')
    <link rel="stylesheet" type="text/css" href="{{asset('css/timepicki.css')}}">
    @endSection()
    <script>
        function show(){
            document.getElementById("schedule").style.display="block";
        }
    </script>
@section('content')
    <div class="page animsition">
        <div class="page-header">
            <h1 class="page-title">Messages</h1>
            <div class="page-header-actions">
                <ol class="breadcrumb">
                    <li><a href="">Home</a></li>
                    <li><a href="javascript:void(0)">Messages</a></li>
                </ol>
            </div>
        </div>
        <div class="page-content">
            <!-- Panel -->
            <div class="panel">
                <div class="panel-body container-fluid">
                    <div class="row row-lg">
                        <div class="col-md-12">
                            <!-- Example Basic -->
                            {!! Form::open(array('url'=>'update', 'method'=>'post','role'=>'search','id'=>'update')) !!}
                            <div class="">
                                    @if( ($errors->any()))
                                        <ul class="alert alert-danger">
                                            @foreach ($errors->all() as $error)
                                                <li class="error_message"> {{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif


                                    @if (session()->has('message'))
                                        <div class="alert alert-success">
                                        {{ session('message') }}
                                        </div>
                                    @endif


                                <div class="table-responsive">

                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Contact</th>
                                            <th>Message</th>
                                            <th>date</th>
                                            <th>Reply</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                            @if(count($posts))
                                                @foreach( $posts  as $post)
                                                    <tr>
                                                        <td><a href="{{ url('messages/'.$post->user_id) }}" style="text-decoration: none; color: rgb(118, 131, 143);">{{ $post->fullname }}</a></td>
                                                        <td><a href="{{ url('messages/'.$post->user_id) }}" style="text-decoration: none; color: rgb(118, 131, 143);">{{ $post->phone }}</a></td>
                                                        {{--<td>{!! html_entity_decode(substr($post->message, 0, 150 ).'...') !!}</td>--}}
                                                        <td>{!! html_entity_decode($post->message) !!}</td>
                                                        <td>{{ $post->created_at }}</td>
                                                        <td>
                                                        <i class="fa fa-envelope-o" onclick="window.location='{{ url('messages/'.$post->user_id) }}'" style="cursor: pointer;"></i>
                                                        <br/>
                                                         <a href="#"><i class="fa fa-trash-o"onClick="return confirmChange( {{$post->id}} )" style="cursor: pointer;"></i></a>
                                                        </td>
                                                        <td>
                                                    </tr>
                                                    <script>

                                                          function confirmChange(id)
                                                          {

                                                          var answer  = confirm("Are you sure you want to delete this message?");

                                                          if(answer==true) {
                                                              window.location='DeleteMessage/'+id;

                                                          }else{
                                                              //do something
                                                              return false;
                                                             }
                                                          }
                                                     </script>
                                                @endforeach
                                            @endif

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {!! Form::close() !!}
                            <!-- End Example Basic -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Panel -->
        </div>
    </div>


    @endSection()
@section('footer')
    <script src="{{ asset('js/timepicki.js')}}"></script>
    <script>
        $('#time1').timepicki();
        $('#time2').timepicki();
    </script>
    @endSection()