@extends('layout.base')
@section('head')
    <link rel="stylesheet" type="text/css" href="{{asset('css/timepicki.css')}}">
    @endSection()
    <script>
        function show(){
            document.getElementById("schedule").style.display="block";
        }
    </script>
@section('content')
    <div class="page animsition">
        <div class="page-header">
            <h1 class="page-title">Messaging Portal</h1>
            <div class="page-header-actions">
                <ol class="breadcrumb">
                    <li><a href="">Home</a></li>
                    <li><a href="javascript:void(0)">Tables</a></li>
                    <li class="active">Basic</li>
                </ol>
            </div>
        </div>
        <div class="page-content">
            <!-- Panel -->
            <div class="panel">
                <div class="panel-body container-fluid">
                    <div class="row row-lg">
                        <div class="col-md-12">
                            <div class="modal-dialog">
                                @foreach( $appointments  as $appointment)
                                    {!! Form::open(array('url'=>'send-message', 'method'=>'post','class'=>'modal-content','files'=>true)) !!}
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="exampleFormModalLabel">Send Message</h4>
                                        @if( ($errors->any()))
                                            <ul class="alert alert-danger">
                                                @foreach ($errors->all() as $error)
                                                    <li class="error_message"> {{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        @endif


                                        @if (session()->has('message'))
                                            <div class="alert alert-success">
                                                {{ session('message') }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-lg-12 form-group">
                                                {!! Form::text('phone', $appointment->phone,array('class'=>'form-control')) !!}
                                                {!! Form::hidden('appointment_id',$appointment->appointment_id) !!}
                                            </div>
                                            <div class="col-lg-12 form-group">
                                                {!! Form::text('message_title','',array('class'=>'form-control', 'placeholder'=>'Message Title')) !!}

                                            </div>

                                            <div class="col-lg-12 form-group">
                                                {!! Form::textarea('message_content','',array('class'=>'form-control','placeholder'=>'Message Content')) !!}
                                            </div>

                                            <div class="col-sm-12 pull-right">
                                                <button class="btn btn-primary btn-outline">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                               @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Panel -->
        </div>
    </div>


    @endSection()
    @section('footer')
    <script src="{{ asset('js/timepicki.js')}}"></script>
    <script>
        $('#time1').timepicki();
        $('#time2').timepicki();
    </script>
    @endSection()