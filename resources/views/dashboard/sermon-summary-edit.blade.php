@extends('layout.base')
@section('head')
    <link rel="stylesheet" type="text/css" href="{{asset('css/timepicki.css')}}">
    @endSection()
    <script>
        function show(){
            document.getElementById("schedule").style.display="block";
        }
    </script>
@section('content')
    <div class="page animsition">
        <div class="page-header">
            <h1 class="page-title">Sermon Summary</h1>
            <div class="page-header-actions">
                <ol class="breadcrumb">
                    <li><a href="">Home</a></li>
                    <li><a href="javascript:void(0)">Edit Summary</a></li>
                </ol>
            </div>
        </div>
        <div class="page-content">
            <!-- Panel -->
            <div class="panel">
                <div class="panel-body container-fluid">
                    <div class="row row-lg">
                        <div class="col-md-12">
                            <!-- Example Basic -->
                            <div class="">
                                <h4 class="">Summary</h4>
                                    @if( ($errors->any()))
                                        <ul class="alert alert-danger">
                                            @foreach ($errors->all() as $error)
                                                <li class="error_message"> {{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif


                                    @if (session()->has('message'))
                                        <div class="alert alert-success">
                                        {{ session('message') }}
                                        </div>
                                    @endif

                                <div style="margin-bottom:10px;" class="category">
                                    <a class="btn btn-primary"  data-target="#FormModal" data-toggle="modal"><i class="fa fa-plus-circle"></i> Edit Summary</a>

                                </div>

                            </div>
                            <!-- End Example Basic -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Panel -->
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="FormModal" aria-hidden="false" aria-labelledby="exampleFormModalLabel" role="dialog" tabindex="-1">
        <div class="modal-dialog">
            {!! Form::open(array('url'=>'sermon-summary-edit', 'method'=>'post','class'=>'modal-content','files'=>true)) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="exampleFormModalLabel">Edit Summary</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 form-group">
                    {!! Form::hidden('summary_id',$sermon->summary_id) !!}
                        {!! Form::text('sermon_title',$sermon->summary_title,array('class'=>'form-control', 'placeholder'=>'Sermon Title')) !!}
                    </div>

                    <div class="col-lg-12 form-group">
                       {!! Form::text('preacher',$sermon->preacher,array('class'=>'form-control', 'placeholder'=>'Preacher')) !!}
                    </div>

                    <div class="col-lg-12 form-group">
                       {!! Form::text('sermon_date',$sermon->sermon_date,array('class'=>'form-control', 'placeholder'=>'Sermon Date(8th June, 2016)')) !!}
                    </div>



                    <div class="col-lg-12 form-group">
                        {!! Form::textarea('summary_content',$sermon->summary_content,array('class'=>'form-control','placeholder'=>'Sermon Content','id'=>'editor')) !!}
                    </div>
                    <div class="col-lg-12 form-group">
                        {!! Form::file('pic') !!}
                    </div>



                    <div class="col-sm-12 pull-right">
                        <button class="btn btn-primary btn-outline">Submit</button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <!-- End Modal -->


    @endSection()
@section('footer')
    <script src="//cdn.ckeditor.com/4.5.3/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'editor' );
    </script>
    @endSection()