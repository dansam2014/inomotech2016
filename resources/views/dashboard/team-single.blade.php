@extends('layout.base')
@section('head')
    @endSection()
@section('content')
    <div class="page animsition">
        <div class="page-header">
            <h1 class="page-title">Posts Available</h1>
            <div class="page-header-actions">
                <ol class="breadcrumb">
                    <li><a href="">Home</a></li>
                    <li><a href="javascript:void(0)">Teams</a></li>
                </ol>
            </div>
        </div>
        <div class="page-content">
    </div>

        <div>
        {!! Form::open(array('url'=>'/team/'.$team->team_id, 'method'=>'PUT','class'=>'modal-content','files'=>true)) !!}
    @if( ($errors->any()))
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li class="error_message"> {{ $error }}</li>
            @endforeach
        </ul>
    @endif


    @if (session()->has('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
            <div >

            </div>
            <div style="margin-left: 30px;">
                <h4 >Add Team</h4>
                <div class="row">
                    <div class="col-lg-6 form-group">
                        {!! Form::text('fullname',$team->fullname,array('class'=>'form-control')) !!}
                        {!! Form::hidden('team_id',$team->team_id) !!}
                    </div>

                    <div class="col-lg-6 form-group">
                        {!! Form::text('position',$team->position,array('class'=>'form-control')) !!}
                    </div>
                    <div class="col-lg-12 form-group">
                        {!! Form::textarea('biography',$team->biography,array('class'=>'form-control','id'=>'editor')) !!}
                    </div>

                    <div class="col-lg-6 form-group">
                        {!! Form::text('facebook',$team->facebook,array('class'=>'form-control')) !!}
                    </div>
                    <div class="col-lg-12 form-group">
                        {!! Form::text('twitter',$team->twitter,array('class'=>'form-control')) !!}
                    </div>
                    <div class="col-lg-12 form-group">
                        {!! Form::file('team_image') !!}
                    </div>

                    <div class="col-sm-12 pull-right" >
                        <button class="btn btn-primary btn-outline">Submit</button>
                    </div>

                </div>
            </div>
            {!! Form::close() !!}
        </div>




    @endSection()
@section('footer')
    <script src="//cdn.ckeditor.com/4.5.3/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'editor' );
    </script>
    @endSection()