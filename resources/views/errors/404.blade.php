<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    @include('partials/head')
</head>
<body>

	<!-- PRELOADER -->
        <div id="loader">
			<div class="loader-container">
				<img src="{{ asset('main/images/load.gif')}}" alt="" class="loader-site spinner">
			</div>
		</div>
	<!-- END PRELOADER -->

    <div id="wrapper">
        <div class="topbar">
             @include('partials/top-header')
        </div><!-- end topbar -->

        <header class="header">
            @include('partials/header')<!-- end container -->
        </header><!-- end header -->

        <div class="after-header">
             @include('partials/below-header')
        </div><!-- end after-header -->
        </section><!-- end section -->

        <div class="section page-title lb">
            <div class="container clearfix">
                <div class="title-area pull-left">
                <br/><br/>
                    <h2><strong>Oops!</strong> we couldn't find this Page!</h2></small></h2>
                    <h2>Please check your spelling</h2>
                    <br/>
                   <h2><a href="{{ url('/') }}">Go Home</a></h2></div>
                </div><!-- /.pull-right -->
                <div class="pull-right hidden-xs">

                </div><!-- /.pull-right -->
            </div>
        </div><!-- end page-title -->
        <section class="section lb">
            <div class="container">
                <div class="row">
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->
        <section class="smallsec">
            <div class="container">

            </div><!-- end container -->
        </section><!-- end section -->

        <footer class="footer lb">
           @include('partials/footer')
       </footer><!-- end footer -->

       @include('partials/below-footer')

    <div class="dmtop">Scroll to Top</div>
</div><!-- end wrapper -->

    @include('partials/jsfiles')

</body>

<!-- Mirrored from showwp.com/demos/hosthubs/page-contact.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Jul 2016 00:07:50 GMT -->
</html>