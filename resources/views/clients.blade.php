<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
     @include('partials/head')
</head>
<body>

	<!-- PRELOADER -->
        <div id="loader">
			<div class="loader-container">
				<img src="{{ asset('main/images/load.gif') }}" alt="" class="loader-site spinner">
			</div>
		</div>
	<!-- END PRELOADER -->

    <div id="wrapper">
        <div class="topbar">
          @include('partials/top-header')
        </div><!-- end topbar -->

        <header class="header">
              @include('partials/header')<!-- end container -->
        </header><!-- end header -->

        <div class="after-header">
             @include('partials/below-header')
        </div><!-- end after-header -->

        <section class="section paralbackground page-banner" style="background-image:url('/main/upload/page_banner_07.jpg');" data-img-width="2000" data-img-height="400" data-diff="100">
        </section><!-- end section -->

        <div class="section page-title lb">
            <div class="container clearfix">
                <div class="title-area pull-left">
                    <h2>Our Client <small>We make our clients happy</small></h2>
                </div><!-- /.pull-right -->
                <div class="pull-right hidden-xs">
                    <div class="bread">
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">Clients</li>
                        </ol>
                    </div><!-- end bread -->
                </div><!-- /.pull-right -->
            </div>
        </div><!-- end page-title -->
 <section class="section lb">

                     <div class="container-fluid">
                      <div class="row">
                          <div class="col-md-12">
                              <nav class="portfolio-filter text-center">

                                  <ul>
                                      <li><a class="btn btn-default" href="#" data-filter="*">All</a></li>
                                      <li><a class="btn btn-default" href="#" data-filter=".cat1">Websites</a></li>
                                      <li><a class="btn btn-default" href="#" data-filter=".cat2">Web Apps</a></li>
                                      <li><a class="btn btn-default" href="#" data-filter=".cat3">Mobile Apps</a></li>
                                      <li><a class="btn btn-default" href="#" data-filter=".cat4">Other</a></li>

                                  </ul>

                              </nav>
                          </div>
                      </div>

                      <div id="fourcol" class="portfolio">

                        @foreach($webs as $web)
                            @if($web->category=='website')
                              <div class="pitem item-w1 item-h1 cat1">
                                  <div class="gallery-column">
                                      <div class="post-media entry">
                                         <a href="{{ url('clients/'.$web->client_id) }}"><img src="{{ $web->client_logo }}" alt="" class="img-responsive"></a>
                                      </div><!-- end post-media -->
                                  </div><!-- end gallery-column -->
                              </div><!-- end col -->
                            @elseif($web->category=='web app')
                               <div class="pitem item-w1 item-h1 cat2">
                                  <div class="gallery-column">
                                       <div class="post-media entry">
                                           <a href="{{ url('clients/'.$web->client_id) }}"><img src="{{ $web->client_logo }}" alt="" class="img-responsive"></a>
                                        </div><!-- end post-media -->
                                  </div><!-- end gallery-column -->
                              </div><!-- end col -->
                              @elseif($web->category=='mobile app')
                                 <div class="pitem item-w1 item-h1 cat3">
                                    <div class="gallery-column">
                                         <div class="post-media entry">
                                             <a href="{{ url('clients/'.$web->client_id) }}"><img src="{{ $web->client_logo }}" alt="" class="img-responsive"></a>
                                          </div><!-- end post-media -->
                                    </div><!-- end gallery-column -->
                                </div><!-- end col -->
                                @elseif($web->category=='other')
                                 <div class="pitem item-w1 item-h1 cat4">
                                    <div class="gallery-column">
                                         <div class="post-media entry">
                                             <a href="{{ url('clients/'.$web->client_id) }}"><img src="{{ $web->client_logo }}" alt="" class="img-responsive"></a>
                                          </div><!-- end post-media -->
                                    </div><!-- end gallery-column -->
                                </div><!-- end col -->
                            @else
                            <div class="pitem item-w1 item-h1 cat1 cat2 cat3 cat4">
                              <div class="gallery-column">
                                   <div class="post-media entry">
                                   <a href="{{ url('clients/'.$web->client_id) }}"><img src="{{ $web->client_logo }}" alt="" class="img-responsive"></a>
                                </div><!-- end post-media -->
                              </div><!-- end gallery-column -->
                            </div><!-- end col -->

                            @endif
                        @endforeach



                      </div><!-- end row -->

                      <nav class="clearfix text-center">
                          <ul class="pagination">
                             {!! $webs->render() !!}
                          </ul>
                      </nav>
                  </div><!-- end container -->

              </section><!-- end section -->

        <section class="section testibanner">
            <div class="container-fluid">
                <div class="section-title text-center">
                    <h3>Happy <span>Testimonials</span></h3>
                    <p class="last">
                        Let's see what other's say about HostHubs web hosting company!<br> 10.000+ customers can not be wrong!
                    </p>
                </div><!-- end section-title -->

                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="row testimonials">
                           @foreach($say_all as $say)
                            <div class="col-md-3 col-sm-6 m30">
                                <blockquote>
                                    <p class="clients-words">  {!! html_entity_decode($say->say_content) !!}</p>
                                    <span class="clients-name text-primary">— {{ $say->say_name}}</span>
                                    <img class="img-circle img-thumbnail" src="{{ $say->say_image }}" alt="">
                                </blockquote>
                            </div>
                          @endforeach
                            <!-- <div class="col-md-3 col-sm-6 m30">
                                <blockquote>
                                    <p class="clients-words">HostHubs is an one powerful hosting company! Now with Php contact form. Perfect! Nice design, great support. </p>
                                    <span class="clients-name text-primary">— Factory nn (@facnnteam)</span>
                                    <img class="img-circle img-thumbnail" src="upload/client_02.png" alt="">
                                </blockquote>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <blockquote>
                                    <p  class="clients-words"> It's an amazing hosting!. HostHubs is very easy to edit for a beginner and the design quality excellent.</p>
                                    <span class="clients-name text-primary">— Martin Lorrenso</span>
                                    <img class="img-circle img-thumbnail" src="upload/client_03.png" alt="">
                                </blockquote>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <blockquote>
                                    <p  class="clients-words">Well designed and organized hosting package! with good amount of features available in it. High recommended!</p>
                                    <span class="clients-name text-primary">— Boby Anderson</span>
                                    <img class="img-circle img-thumbnail" src="upload/client_04.png" alt="">
                                </blockquote>
                            </div> -->
                        </div>
                    </div><!--/.col-->  
                </div><!--/.row-->
            </div><!-- end container -->
        </section><!-- end section -->





        <section class="smallsec">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 text-center">
                        <h3>Contact us and let's do business together</h3>
                    </div>
                    <div class="col-md-4 text-center">
                        <a href="{{ url(('contact')) }}" class="btn btn-primary btn-lg"><i class="fa fa-comments-o"></i> Contact Us</a>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->

        <footer class="footer lb">
            @include('partials/footer')
        </footer><!-- end footer -->

        @include('partials/below-footer')

    <div class="dmtop">Scroll to Top</div>
</div><!-- end wrapper -->

    <!-- Main Scripts-->
    <script src="{{ asset('main/js/jquery.js') }}"></script>
    <script src="{{ asset('main/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('main/js/plugins.js') }}"></script>
    <script src="{{ asset('main/js/isotope.js') }}"></script>
    <script src="{{ asset('main/js/imagesloaded.pkgd.js') }}"></script>
    <script src="{{ asset('main/js/four-col-portfolio.js') }}"></script>

</body>
</html>