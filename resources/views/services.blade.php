<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
@include('partials/head')
</head>
<body>

	<!-- PRELOADER -->
        <div id="loader">
			<div class="loader-container">
				<img src="{{ asset('main/images/load.gif')}}" alt="" class="loader-site spinner">
			</div>
		</div>
	<!-- END PRELOADER -->

    <div id="wrapper">
        <div class="topbar">
             @include('partials/top-header')
        </div><!-- end topbar -->

        <header class="header">
           @include('partials/header')
        </header><!-- end header -->

        <div class="after-header">
            @include('partials/below-header')
        </div><!-- end after-header -->

        <section class="section paralbackground page-banner" style="background-image:url('main/upload/page_banner_06.jpg');" data-img-width="2000" data-img-height="400" data-diff="100">
        </section><!-- end section -->

        <div class="section page-title">
            <div class="container clearfix">
                <div class="title-area pull-left">
                    <h2>Custom Works <small>This is services page of IMT</small></h2>
                </div><!-- /.pull-right -->
                <div class="pull-right hidden-xs">
                    <div class="bread">
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">Services</li>
                        </ol>
                    </div><!-- end bread -->
                </div><!-- /.pull-right -->
            </div>
        </div><!-- end page-title -->

        <section class="section lb">
            <div class="container">
                <div class="row custom-services">
                    <div class="col-md-6">
                        <h2>Call IMT now! </h2>
                    </div><!-- end col -->

                    <div class="col-md-3">
                        <p>Do you want to take advantage of our special offers? Please drop a message.</p>
                    </div><!-- end col -->

                    <div class="col-md-3">
                        <a href="{{ url('contact')}}" class="btn btn-primary btn-lg btn-block">+233-248755510</a>
                    </div>
                </div><!-- end custom-services -->
            </div><!-- end container -->
        </section><!-- end section -->

        <section class="section">
            <div class="container">
                <div class="section-title text-center">
                    <h3>Service <span>Overviews</span></h3>
                    <p class="last">
                        Our greatest concern is to provide the best services to people all over Africa and the world at large. Providing them with quality solutions that will increase efficiency and foster growth and development. Some of the services we render are as follows.
                    </p>
                </div><!-- end section-title -->

                <div class="row rounded-services text-center">
                    <div class="col-md-4 col-sm-6">
                        <div class="box">
                            <div class="hi-icon wow fadeIn">
                                <i class="fa fa-laptop"></i>
                            </div>
                            <h3>Web Development</h3>
                            <p>We provide the most effective and efficient software application that ensure maximum security and expedite business transactions. Our greatest concern is to provide our clients the best services.
</p>
                        </div><!-- end box -->
                    </div>
                    <div class="col-md-4 col-sm-6 ">
                        <div class="box">
                            <div class="hi-icon wow fadeIn">
                            <i class="fa fa-mobile"></i>
                            </div>
                            <h3>Mobile Development</h3>
                            <p>We work with industry standards to help in building mobile applications and implementation strategies. </p>
                        </div><!-- end box -->
                    </div>

                    <div class="col-md-4 col-sm-6">
                        <div class="box">
                            <div class="hi-icon wow fadeIn">
                            <i class="fa fa-level-up"></i>
                            </div>
                            <h3>ICT Advisory & Consultancy</h3>
                            <p>IMT's stalk and trade is providing innovative, cutting edge, bespoke solutions in terms of ICT advisory, consultancy and general restructuring of ICT policies and procedures of Organizations/Institutions. </p>
                        </div><!-- end box -->
                    </div><!-- end col -->
                </div><!-- end row -->

                <hr class="invis1">

                <div class="row rounded-services text-center">
                    <div class="col-md-4 col-sm-6">
                        <div class="box">
                            <div class="hi-icon wow fadeIn">
                                <i class="fa fa-wrench"></i>
                            </div>
                            <h3>Technical Support & Consultancy</h3>
                            <p>Technical Support is user-friendly assistance to help resolve any IT related technical problems. The technical support team are familiar with resolving the most simple to the most complex user and computer systems issues.</p>
                        </div><!-- end box -->
                    </div>
                    <div class="col-md-4 col-sm-6 ">
                        <div class="box">
                            <div class="hi-icon wow fadeIn">
                            <i class="fa fa-lightbulb-o yellow"></i>
                            </div>
                            <h3>Software Testing</h3>
                            <p>We are the best software quality assurance testing company in the country. We ensure that developed software adheres and complies with defined or standardized quality specifications.</p>
                        </div><!-- end box -->
                    </div>

                    <div class="col-md-4 col-sm-6">
                        <div class="box">
                            <div class="hi-icon wow fadeIn">
                            <i class="fa fa-life-ring green"></i>
                            </div>
                            <h3>Motion Graphics Design</h3>
                            <p>We have a reputation of designing the most comprehensive motions graphic. We ensure that our designs adheres and complies with the standards of the client. </p>
                        </div><!-- end box -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section>

        <section class="section testibanner">
            <div class="container-fluid">
                <div class="section-title text-center">
                    <h3>Happy <span>Testimonials</span></h3>
                    <p class="last">
                        Let's see what other's say about HostHubs web hosting company!<br> 10.000+ customers can not be wrong!
                    </p>
                </div><!-- end section-title -->

                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="row testimonials">
                        @foreach($say_all as $say)
                            <div class="col-md-3 col-sm-6 m30">
                                <blockquote>
                                    <p class="clients-words"> {!! html_entity_decode($say->say_content) !!}</p>
                                    <span class="clients-name text-primary">— {{ $say->say_name}}</span>
                                    <img class="img-circle img-thumbnail" src="{{ $say->say_image }}" alt="">
                                </blockquote>
                            </div>
                        @endforeach
                        </div>
                    </div><!--/.col-->  
                </div><!--/.row-->
            </div><!-- end container -->
        </section><!-- end section -->

        <section class="section lb overflow">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="big-title">
                            <span>Our Latest Clients</span>
                            </h3>
                        </div><!-- end big-title -->

                        <div class="hfeatures">
                            <p>We work in partnership with our clients, advising them to use information technology in order to meet their business objectives or overcome problems. We help to improve the structure and efficiency of IT systems in various organizations. We provide strategic guidance to clients with regard to technology, IT infrastructures and enabling major business processes through enhancements to IT.</p>

                            <ul class="work-elements">
                                @foreach( $client_1 as $clients_1)
                                <li>
                                    <div class="box GrayScale">
                                        <div class="icon-container">
                                           <a href="{{ url('clients/'.$clients_1->client_id) }}"><img src="{{ $clients_1->client_logo }}" alt="" class="img-responsive"></a>
                                        </div>
                                    </div><!-- end featurebox -->
                                </li><!-- end col -->
                                @endforeach

                                
                            </ul>
                        </div><!-- end hfeatures -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->

            <div class="image-box hidden-sm hidden-xs wow slideInRight">
                <img alt="" src="{{ asset('main/upload/device_07.png') }}">
            </div>
        </section><!-- end section -->


        <section class="smallsec">
             <div class="container">
                            <div class="row">
                                <div class="col-md-8 text-center">
                                    <h3>Contact us and let's do business together</h3>
                                </div>
                                <div class="col-md-4 text-center">
                                    <a href="{{ url(('contact')) }}" class="btn btn-primary btn-lg"><i class="fa fa-comments-o"></i> Contact Us</a>
                                </div><!-- end col -->
                            </div><!-- end row -->
                        </div><!-- end container -->
        </section><!-- end section -->

        <footer class="footer lb">
                   @include('partials/footer')
        </footer><!-- end footer -->

               @include('partials/below-footer')

    <div class="dmtop">Scroll to Top</div>
</div><!-- end wrapper -->
@include('partials/jsfiles')
</body>
</html>