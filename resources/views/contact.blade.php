<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    @include('partials/head')
</head>
<body>

	<!-- PRELOADER -->
        <div id="loader">
			<div class="loader-container">
				<img src="{{ asset('main/images/load.gif')}}" alt="" class="loader-site spinner">
			</div>
		</div>
	<!-- END PRELOADER -->

    <div id="wrapper">
        <div class="topbar">
             @include('partials/top-header')
        </div><!-- end topbar -->

        <header class="header">
            @include('partials/header')<!-- end container -->
        </header><!-- end header -->

        <div class="after-header">
             @include('partials/below-header')
        </div><!-- end after-header -->

        <section class="section paralbackground page-banner" style="background-image:url('/main/upload/page_banner_04.jpg');" data-img-width="2000" data-img-height="400" data-diff="100">
        </section><!-- end section -->

        <div class="section page-title lb">
            <div class="container clearfix">
                <div class="title-area pull-left">
                    <h2>Contact Center <small>Welcome to the IMT's Support Center</small></h2>
                </div><!-- /.pull-right -->
                <div class="pull-right hidden-xs">
                    <div class="bread">
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">Contact</li>
                        </ol>
                    </div><!-- end bread -->
                </div><!-- /.pull-right -->
            </div>
        </div><!-- end page-title -->
        <section class="section lb">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="support-box whitebg">
                            <h3>Postal Support</h3>
                            <p>PO Box KN 4023,
                            #14 Alema Avenue, Airport Res. Area<br>
                            <a href="mailto:inomotech.com">hello@inomotech.com</a></p>
                        </div><!-- end support-box -->

                        <hr>

                        <div class="support-box whitebg">
                            <h3>Online Chat</h3>
                            <p>You can get support by contacting our live support through active operator on the lower right page located.</p>

                        </div><!-- end support-box -->
                    </div><!-- end col -->
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="wbp">
                            <div class="small-title">
                                <h3>Contact Form</h3>
                                <hr>
                            </div><!-- end big-title -->

                            <br>

                            <div class="contact_form offical_form">
                              @if($errors->any())
                                <ul class="alert alert-danger">
                                        @foreach($errors->all() as $error)
                                         <li class="error_message">{{ $error }}</li>
                                        @endforeach
                                 </ul>
                        @endif

                        @if(session()->has('message'))
                                <div class="alert alert-success">
                                        {{ session('message') }}
                                 </div>
                        @endif
                                <div id="message"></div>
                                {!! Form::open(array('url'=>url('contact'),'class'=>'row','method'=>'POST')) !!}
                                    <div class="col-md-12">
                                  {!! Form::text('fullname','',array('class'=>'form-control required','id'=>'name','placeholder'=>'Name')) !!}
                                   
                                   {!! Form::text('email','',array('class'=>'form-control required','id'=>'email','placeholder'=>'Email')) !!}

                                   {!! Form::text('phone','',array('class'=>'form-control required','id'=>'phone','placeholder'=>'Phone')) !!}

                                    {!! Form::text('subject','',array('class'=>'form-control required','id'=>'subject','placeholder'=>'Subject')) !!} 
                                    
                                    {!! Form::textarea('message','',array('class'=>'form-control','rows'=>'6','id'=>'comments','placeholder'=>'Message')) !!}

                                    <button type="submit" class="btn btn-primary"> Send</button>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div><!-- end wbp -->
                    </div><!-- end col -->
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="support-box whitebg">
                            <h3>Technical Support</h3>
                            <p>You can send support requests via our contact form, or contact us on our number +233 244 139 937</p>
                        </div><!-- end support-box -->

                        <hr>

                        <div class="support-box whitebg">
                            <h3>Sales Department</h3>
                            <p>For any sale question let's decide together with appropriate service for you in consultation with sales team. Call us on +233 248 755 510</p>

                        </div><!-- end support-box -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->
        <section class="smallsec">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 text-center">
                        <h3>Have a question for our packages?</h3>
                    </div>
                    <div class="col-md-4 text-center">
                        <a href="#" class="btn btn-primary btn-lg"><i class="fa fa-comments-o"></i> +233 248 755 510</a>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->

        <footer class="footer lb">
           @include('partials/footer')
       </footer><!-- end footer -->

       @include('partials/below-footer')

    <div class="dmtop">Scroll to Top</div>
</div><!-- end wrapper -->

    @include('partials/jsfiles')

</body>

<!-- Mirrored from showwp.com/demos/hosthubs/page-contact.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Jul 2016 00:07:50 GMT -->
</html>