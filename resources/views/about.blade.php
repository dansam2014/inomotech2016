<!DOCTYPE html>
<html lang="en"> 
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
@include('partials/head')
</head>
<body>

	<!-- PRELOADER -->
        <div id="loader">
			<div class="loader-container">
				<img src="{{ asset('main/images/load.gif') }}" alt="" class="loader-site spinner">
			</div>
		</div>
	<!-- END PRELOADER -->

    <div id="wrapper">
        <div class="topbar">
             @include('partials/top-header')
        </div><!-- end topbar -->

        <header class="header">
           @include('partials/header')
        </header><!-- end header -->

        <div class="after-header">
            @include('partials/below-header')
        </div><!-- end after-header -->

        <section class="section fullheight paralbackground parallax" style="background-image:url('/main/upload/about.jpg');" data-img-width="3000" data-img-height="2000" data-diff="100">
            <div class="frameT">
                <div class="frameTC">
                    <div class="content">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <h1>We Are IMT</h1>
                                </div><!-- end col -->
                            </div><!-- end row -->
                        </div><!-- end container -->
                    </div><!-- end content -->
                </div><!-- end frameTC -->
            </div><!-- end frameT -->
        </section><!-- end section -->

        <section class="section lb">
            <div class="container">
                <div class="section-title text-center">
                    <h3>Team <span>Members</span></h3>
                    <p class="last">
                        We are made up of the best brains in the industry.<br> Customer satisfaction is our hallmark!!
                    </p>
                </div><!-- end section-title -->
        
                <div class="row team-members text-center">
                @foreach($team as $member)
                    <div class="col-md-3 col-sm-6 col-xs-12 team-member wow fadeIn">
                        <div class="entry">
                            <img src="{{ $member->team_image }}" alt="" class="img-responsive">
                            <div class="magnifier">
                                <div class="visible-buttons">
                                    <span><a data-placement="bottom" data-toggle="tooltip" title="Follow Us" href="#"><i class="fa fa-twitter"></i></a></span>
                                    <span><a data-placement="bottom" data-toggle="tooltip" title="Follow Us" href="#"><i class="fa fa-facebook"></i></a></span>
                                    <span><a data-placement="bottom" data-toggle="tooltip" title="Follow Us" href="#"><i class="fa fa-google-plus"></i></a></span>
                                    <span><a data-placement="bottom" data-toggle="tooltip" title="Follow Us" href="#"><i class="fa fa-pinterest"></i></a></span>
                                </div>
                            </div>
                        </div><!-- end entry -->
                        <h4>{{ $member->fullname }}</h4>
                        <small>{{ $member->position }}</small>
                        <p>{!! html_entity_decode(substr($member->biography,0,250).'...') !!}</p>
                    </div><!-- end col -->
                @endforeach
                  
<!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->

        <section class="section nopadding clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-sm-12 customsection hidden-sm hidden-xs">
                        <img src="{{ asset('main/upload/section_02.jpg')}}" alt="images">
                        <div class="text_container">
                            <span class="logo"></span>
                            <h4>What We<br>Offer?</h4>
                            <p>Our greatest concern is to provide the best services to people all over Africa and the world at large, & Providing them with quality solutions that will increase efficiency and foster growth and development</p>
                            <a href="{{ url('contact')}}" class="btn btn-primary">Contact Us</a>
                        </div>
                    </div>

                    <div class="col-md-7 col-sm-12">
                        <div class="box-feature-full clearfix">
                           <div class="vertical-elements">
                                <div class="box">
                                    <div class="icon-container alignleft">
                                        <img src="{{ asset('main/images/icons/features_box_01.png')}}" alt="" class="img-responsive">
                                    </div>

                                    <div class="feature-desc">
                                        <h4>Web/Mobile Application Development</h4>
                                        <p>We are the best in the Web/Mobile development field. We can be trusted!</p>
                                    </div><!-- end desc -->
                                </div><!-- end featurebox -->

                                <hr>

                                <div class="box">
                                    <div class="icon-container alignleft">
                                        <img src="{{ asset('main/images/icons/features_box_02.png')}}" alt="" class="img-responsive">
                                    </div>

                                    <div class="feature-desc">
                                        <h4>IT Consultancy/Business Automation</h4>
                                        <p>We provide the most effective consultancy in the country.</p>
                                    </div><!-- end desc -->
                                </div><!-- end featurebox -->

                                <hr>

                                <div class="box">
                                    <div class="icon-container alignleft">
                                        <img src="{{ asset('main/images/icons/features_box_03.png')}}" alt="" class="img-responsive">
                                    </div>

                                    <div class="feature-desc">
                                        <h4>Motion Graphics / Software testing</h4>
                                        <p>We are the best software quality assurance testing company in Africa</p>
                                    </div><!-- end desc -->
                                </div><!-- end featurebox -->
                            </div><!-- end vertical -->
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- end section -->

        <section class="section lb">
            <div class="container">
                <div class="row custom-services">
                    <div class="col-md-6">
                        <h2>Have Any Questions?</h2>
                    </div><!-- end col -->

                    <div class="col-md-3">
                        <p>Do you want to take advantage of our special offers? Please call us today.</p>
                    </div><!-- end col -->

                    <div class="col-md-3">
                        <a href="{{ url('contact')}}" class="btn btn-primary btn-lg btn-block"><i class="fa fa-phone"></i> +233-248755510</a>
                    </div>
                </div><!-- end custom-services -->
            </div><!-- end container -->
        </section><!-- end section -->

         <section class="section searchbanner1 lightversion">
                    <div class="container">
                        <div class="section-title text-center">
                            <h3>Company <span>Statistics</span></h3>
                        </div><!-- end section-title -->
                        <div class="row services-list hover-services text-center">
                            <div class="col-md-3 col-sm-6 wow fadeIn">
                                <div class="box">
                                    <i class="fa fa-thumbs-up"></i>
                                    <h3>Project Completed</h3>
                                    <p class="stat-count">250</p>
                                </div><!-- end box -->
                            </div>

                            <div class="col-md-3 col-sm-6 wow fadeIn">
                                <div class="box">
                                    <i class="fa fa-smile-o"></i>
                                    <h3>Happy Clients</h3>
                                    <p class="stat-count">350</p>
                                </div><!-- end box -->
                            </div>

                            <div class="col-md-3 col-sm-6 wow fadeIn">
                                <div class="box">
                                    <i class="fa fa-laptop"></i>
                                    <h3>Web Apps</h3>
                                    <p class="stat-count">45</p>
                                </div><!-- end box -->
                            </div>

                            <div class="col-md-3 col-sm-6 wow fadeIn">
                                <div class="box">
                                    <i class="fa fa-mobile"></i>
                                    <h3>Mobile Apps</h3>
                                    <p class="stat-count">10</p>
                                </div><!-- end box -->
                            </div><!-- end col -->
                        </div>
                    </div><!-- end container -->
                </section><!-- end section -->

        <section class="section lb">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-sm-12 col-xs-12">
                        <div class="wbp">
                            <div class="small-title">
                                <h3>Email Support</h3>
                                <hr>
                            </div><!-- end big-title -->

                            <div class="email-widget">
                                <p>For your other questions please use below email addresses.</p>
                                <ul class="check-list">
                                    <li><a href="mailto:support@inomotech.com">support@inomotech.com</a></li>
                                    <li><a href="mailto:sales@inomotech.com">sales@inomotech.com</a></li>
                                    <li><a href="mailto:hello@inomotech.com">hello@inomotech.com</a></li>
                                </ul><!-- end check -->
                            </div><!-- end email widget -->
                        </div><!-- end wbp -->    
                    </div><!-- end col -->

                    <div class="col-md-7 col-sm-12 col-xs-12">
                        <div class="wbp">
                            <div class="small-title">
                                <h3>Contact Form</h3>
                                <hr>
                            </div><!-- end big-title -->
                               @if($errors->any())
                                <ul class="alert alert-danger">
                                        @foreach($errors->all() as $error)
                                         <li class="error_message">{{ $error }}</li>
                                        @endforeach
                                 </ul>
                        @endif

                        @if(session()->has('message'))
                                <div class="alert alert-success">
                                        {{ session('message') }}
                                 </div>
                        @endif

                            <div class="contact_form commentform">
                                {!! Form::open(array('url'=>url('about'),'class'=>'row','method'=>'POST')) !!}
                                   
                                    <div class="col-md-4 col-sm-12">
                                        <label>Name <span class="required">*</span></label>
                                        {!! Form::text('fullname','',array('class'=>'form-control required','id'=>'name','placeholder'=>'')) !!}      
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <label>Email <span class="required">*</span></label>
                                        {!! Form::text('email','',array('class'=>'form-control required','id'=>'email','placeholder'=>'')) !!}
                                        
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <label>Phone</label>
                                        {!! Form::text('phone','',array('class'=>'form-control required','id'=>'email','placeholder'=>'')) !!}
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <label>Messsage<span class="required">*</span></label>
                                        {!! Form::textarea('message','',array('class'=>'form-control','rows'=>'6','id'=>'comments','placeholder'=>'Message')) !!}
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <button type="submit" value="SEND" id="submit" class="btn btn-primary"> Submit</button>
                                    </div>
                                {!! Form::close() !!}
                            </div><!-- end commentform -->
                        </div><!-- end wbp -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->

        <section class="smallsec">
            <div class="container">
                           <div class="row">
                               <div class="col-md-8 text-center">
                                   <h3>Contact us and let's do business together</h3>
                               </div>
                               <div class="col-md-4 text-center">
                                   <a href="{{ url(('contact')) }}" class="btn btn-primary btn-lg"><i class="fa fa-comments-o"></i> Contact Us</a>
                               </div><!-- end col -->
                           </div><!-- end row -->
                       </div><!-- end container -->
        </section><!-- end section -->

         <footer class="footer lb">
                    @include('partials/footer')
           </footer><!-- end footer -->

                @include('partials/below-footer')
    <div class="dmtop">Scroll to Top</div>
</div><!-- end wrapper -->

    <!-- Main Scripts-->
   @include('partials/jsfiles')

</body>

<!-- Mirrored from showwp.com/demos/hosthubs/page-about.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Jul 2016 00:06:52 GMT -->
</html>