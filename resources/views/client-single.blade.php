<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    @include('partials/head')
</head>
<body>

	<!-- PRELOADER -->
        <div id="loader">
			<div class="loader-container">
				<img src="{{ asset('main/images/load.gif')}}" alt="" class="loader-site spinner">
			</div>
		</div>
	<!-- END PRELOADER -->

    <div id="wrapper">
        <div class="topbar">
             @include('partials/top-header')
        </div><!-- end topbar -->

        <header class="header">
            @include('partials/header')<!-- end container -->
        </header><!-- end header -->

        <div class="after-header">
            @include('partials/below-header')
        </div><!-- end after-header -->

        <section class="section paralbackground page-banner" style="background-image:url('/main/upload/page_banner_07.jpg');" data-img-width="2000" data-img-height="400" data-diff="100">
        </section><!-- end section -->

        <div class="section page-title">
            <div class="container clearfix">
                <div class="title-area pull-left">
                    <h2>Our Client<small></small></h2>
                </div><!-- /.pull-right -->
                <div class="pull-right hidden-xs">
                    <div class="bread">
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">Client</li>
                        </ol>
                    </div><!-- end bread -->
                </div><!-- /.pull-right -->
            </div>
        </div><!-- end page-title -->
     
        <section class="section lb">
            <div class="container">
                <div class="row text-left box">
                    <div class="col-md-6 col-sm-6 col-xs-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.1s">
                        <div class="wbp">
                            <div class="blog-image">
                                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                   <img src="{{ $client->client_image  }}" style="width: 100%;">
                                </div>
                            </div><!-- end blog-image --> 
                        </div><!-- end module -->
                    </div><!-- end col -->
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="singlegallery wbp">
                            <div class="small-title">
                                <h3>{{ strtoupper($client->name)  }}</h3>
                                <hr>
                            </div>
                            <div class="gallery-desc">
                               <p>{!! html_entity_decode($client->description) !!}</p>
                                <div class="workinghours clearfix">
                                    <ul>
                                        <li>Category <span><a href="#">{{ $client->category }}</a></span></li>
                                        @if($client->category=='website')
                                        <li>Client Website <span><a href="{{ $client->link }}" target="_blank">{{ $client->link }}</a></span></li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div><!-- end welcome -->
                    </div><!-- end col -->
                </div><!-- end row -->

                <hr class="invis1">

                <nav>
                    <ul class="pager clearfix">
                    <li class="pull-left"><a href="#"><i class="fa fa-angle-left"></i></a></li>
                    <li class="pull-right"><a href="#"><i class="fa fa-angle-right"></i></a></li>
                    </ul>
                </nav>
            </div><!-- end container -->
        </section><!-- end section -->

        <section class="smallsec">
             <div class="container">
                            <div class="row">
                                <div class="col-md-8 text-center">
                                    <h3>Contact us and let's do business together</h3>
                                </div>
                                <div class="col-md-4 text-center">
                                    <a href="{{ url(('contact')) }}" class="btn btn-primary btn-lg"><i class="fa fa-comments-o"></i> Contact Us</a>
                                </div><!-- end col -->
                            </div><!-- end row -->
                        </div><!-- end container -->
        </section><!-- end section -->

        <footer class="footer lb">
           @include('partials/footer')
       </footer><!-- end footer -->

               @include('partials/below-footer')
    <div class="dmtop">Scroll to Top</div>
</div><!-- end wrapper -->

    <!-- Main Scripts-->
    <script src="{{ asset('main/js/jquery.js') }}"></script>
    <script src="{{ asset('main/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('main/js/plugins.js')}}"></script>
    <script src="{{ asset('main/js/isotope.js') }}"></script>
    <script src="{{ asset('main/js/imagesloaded.pkgd.js') }}"></script>
    <script src=""></script>

</body>


</html>