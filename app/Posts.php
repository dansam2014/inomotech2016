<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    protected $primaryKey = "post_id";
    protected $fillable = ['post_title','post_content','category','post_image'];


    public static function uploadImage($file)
    {
        if (! empty($file)) {
            $ext  = $file->getClientOriginalExtension();
            $dir = public_path().'/uploads/';
            $path = '/uploads/';

            $filename = uniqid();
            $uploadedfile = $filename.".{$ext}";

            if($file->move($dir, $uploadedfile)){
                return $path.$uploadedfile;
            }
        }
        return '';
    }

    public function comments()
    {
        return $this->hasMany('App\Comment', 'post_id', 'post_id');
    }

    public function replies()
    {
        return $this->hasMany('App\Comment', 'parent_id', 'id');
    }


}
