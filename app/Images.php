<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    public static function uploadImage($file)
    {
        if (! empty($file)) {
            $ext  = $file->getClientOriginalExtension();
            $dir = public_path().'/uploads/';
            $path = '/uploads/';

            $filename = uniqid();
            $uploadedfile = $filename.".{$ext}";

            if($file->move($dir, $uploadedfile)){
                return $path.$uploadedfile;
            }
        }
        return '';
    }

}
