<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Say extends Model
{
    protected $fillable = ['say_name','say_content','say_image'];

    public static function UploadImage($file)
    {
        if(!empty($file)){
            $ext = $file->getClientOriginalExtension();
            $dir =public_path().'/uploads';
            $path= '/uploads/';

            $filename = uniqid();
            $uploadedfile=$filename.".{$ext}";

            if($file->move($dir,$uploadedfile)){
                return $path.$uploadedfile;
            }
        }

        return '';

    }
}
