<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teams extends Model
{
    protected $primaryKey = "team_id";
    protected $fillable = ['fullname','position','biography','facebook','twitter','team_image'];
    public static function uploadImage($file)
    {
        if (! empty($file)) {
            $ext  = $file->getClientOriginalExtension();
            $dir = public_path().'/uploads/';
            $path = '/uploads/';

            $filename = uniqid();
            $uploadedfile = $filename.".{$ext}";

            if($file->move($dir, $uploadedfile)){
                return $path.$uploadedfile;
            }
        }
        return '';
    }
}
