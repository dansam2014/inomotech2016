<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Carbon;

class PeriodAvailable extends Model
{
    public $timestamps = false;

    public $days = array(
    		'Sunday'=>'Sunday',
    		'Monday'=>'Monday',
    		'Tuesday'=>'Tuesday',
    		'Wednesday'=>'Wednesday',
    		'Thursday'=>'Thursday',
    		'Friday'=>'Friday',
    		'Saturday'=>'Saturday'
    	);

    public function getDayAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d', $date)->format('D, d M Y');
    }
}
