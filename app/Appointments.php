<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointments extends Model
{
    protected $primaryKey = "appointment_id";

    // public function getDesiredDateAttribute($date)
    // {
    // 	return \Carbon\Carbon::createFromFormat('d/m/Y', $date);
    // }

    public function users()
    {
    	return $this->belongsTo('App\User', 'user_id', 'user_id');
    }
}
