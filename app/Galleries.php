<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Galleries extends Model
{
    public function connect_galleries()
    {
        return $this->hasMany('App\Images', 'gallery_id', 'gallery_id');
    }
}
