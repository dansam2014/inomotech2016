<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shika_advices extends Model
{
    protected $primaryKey = "user_id";
    //protected $primaryKey = 'user_id';
    public function connect_solutions()
    {
        return $this->belongsTo('App\Solutions', 'user_id', 'user_id');
    }
    public function comment_reply()
    {
        return $this->hasMany('App\Comment_replies', 'comment_id', 'comment_id');
    }
}
