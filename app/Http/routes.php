<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// Route::get('test',function(){
// 	return Hash::make('@Secured($9028)##$$23');
// });

Route::group(['domain'=>config('app.admin_url')], function (){
    Route::get('login', 'LoginController@index');
    Route::post('login', 'LoginController@signin');

    Route::group(array('middleware'=>'auth'), function(){
        Route::get('/', 'DashboardController@index');
        Route::post('periods', 'PeriodsController@store');
        Route::get('periods', 'PeriodsController@index');
        Route::get('faq', 'FaqController@index');
        Route::post('faq', 'FaqController@store');
        Route::get('faq/{id}','FaqController@destroy');
        Route::get('faq-single/{id}','FaqController@viewFaqupdate');
        Route::put('faq-single/{id}','FaqController@editFaq');


        Route::get('posts', 'PostsController@index');
        Route::get('posts1', 'PostsController@index1');
        Route::get('posts-single/{id}', 'PostsController@postSingle');
        Route::put('posts-single/{id}', 'PostsController@editPosts');

        Route::post('posts', 'PostsController@store');
        Route::post('update_post', 'PostsController@update');
        Route::get('unpublish', 'PostsController@unpublish');
        Route::post('unpublish-posts', 'PostsController@unpublishPosts');
        Route::get('team-category', 'TeamController@categoryView');
        Route::post('team-category', 'TeamController@positionCat');
        Route::get('team-category/{id}', 'TeamController@destroy');
        Route::get('team','TeamController@index');
        Route::post('team','TeamController@store');
        Route::get('team/{id}','TeamController@teamSingl');
        Route::put('team/{id}','TeamController@editTeam');

        Route::get('teamDestroy/{id}', 'TeamController@destroyTeams');
        Route::get('view-appointments','AppointmentController@viewAppointment');
        Route::get('view-appointments/{id}', 'AppointmentController@destroy');
        Route::get('appointment-single/{id}','AppointmentController@singleAppointment');
        Route::get('send-message/{id}','AppointmentController@sendMessage');
        Route::post('send-message','AppointmentController@storeMessage');
        Route::post('appointment-status','AppointmentController@update');
        Route::get('periods/{id}','PeriodsController@destroy');
        Route::get('posts-category', 'PostsController@categoryView');
        Route::post('posts-category', 'PostsController@postCategory');
        Route::get('posts-category/{id}', 'PostsController@destroy');
        Route::get('messages', 'IssuesController@messages');
        Route::get('messages/{id}', 'IssuesController@messageItem');
        Route::get('message/{id}', 'IssuesController@message');
        Route::post('message', 'IssuesController@replyStore');
        Route::get('commentsComment','ShikaAdvicesController@CommentsCmt');
        Route::post('delete_comment_comment', 'ShikaAdvicesController@DeleteComment');
        Route::post('update', 'ShikaAdvicesController@update');
        Route::get('inbox-single/{id}','ShikaAdvicesController@inboxSingle');
        Route::get('outbox/{id}','ShikaAdvicesController@outbox');
        Route::post('shika-reply','ShikaAdvicesController@shikaReplies');
        Route::get('outbox','ShikaAdvicesController@OutboxView');
        Route::get('outbox-single/{id}','ShikaAdvicesController@outboxSingle');
        Route::get('reply/{id}','ShikaAdvicesController@reply');
        Route::post('reply_store','ShikaAdvicesController@reply_store');
        Route::get('loggout', 'LoginController@Logout');
        Route::get('gallery','GalleryController@admin_index');
        Route::post('album','GalleryController@Album');
        Route::get('search','SearchController@search_terms');
        Route::get('subscribers','SearchController@subscribers');
        Route::get('del','SearchController@SearchDelete');

        Route::get('sermon-summary','SermonSummaryController@adminSermon');
        Route::post('sermon-summary','SermonSummaryController@store');
        Route::get('sermon-summary-edit/{id}','SermonSummaryController@Edit');
        Route::post('sermon-summary-edit','SermonSummaryController@EditStore');

        Route::get('client-category', 'ClientsController@cientCat');
        Route::post('client-category', 'ClientsController@cientCatPosts');
        Route::get('client-category/{id}', 'ClientsController@delCategory');
        Route::get('clients', 'ClientsController@clientView');
        Route::post('clients', 'ClientsController@clientSave');
        Route::get('client-single/{id}', 'ClientsController@ClientSingleAdmin');
        Route::post('clientSingle', 'ClientsController@ClientEdit');
        Route::get('DeleteClient/{id}', 'ClientsController@ClientDeletion');

        Route::get('say','TestimonalController@index');
        Route::post('say','TestimonalController@store');

        Route::get('policy','PolicyController@index');
        Route::post('policy','PolicyController@store');

        Route::get('DeleteMessage/{id}','IssuesController@DeleteMessageSingle');



    });

});

Route::get('index','HomePageController@index');
Route::get('/','HomePageController@index1');

Route::get('about','TeamController@about');
Route::get('contact','ContactController@index');
Route::post('contact','ContactController@store_from_contact_page');
Route::post('about','ContactController@store');
Route::get('services','ServicesController@index');
Route::get('clients','ClientsController@index');

Route::get('page/{id}','PolicyController@policySingle');
Route::get('sitemap','SitemapController@sitemap');

Route::get('clients/{id}','ClientsController@clientSingle');
Route::get('blogs','BlogController@index');
Route::get('blogs/{id}','BlogController@blogSingle');
Route::post('news','HomePageController@Newsletter');
Route::post('customerSupport','CustomerSupportController@customerSupportSave');

Route::post('search','SearchController@search');
Route::get('search','SearchController@index');
Route::get('DeleteSearchTerm/{id}','SearchController@DeleteSearchTerms');

