<?php

namespace App\Http\Controllers;

use App\CustomerSupports;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;

class CustomerSupportController extends Controller
{
    public function customerSupportSave(Requests\CustorSupportRequest $request){
        $customer = new CustomerSupports();
        $customer->cus_id = randId();
        $customer->fullname = Input::get('fullname');
        $customer->phone = Input::get('phone');
        $customer->message = Input::get('message');
        $customer->save();

        $num1 = urlencode('0244139937');
        $num2 = urlencode('0248755510');

        $mess = urlencode('Message from '.Input::get('fullname').' of phone number '.Input::get('phone').' Message: '.Input::get('message'));

        sendTextInomotech($num1, $mess);
        //sendTextInomotech($num2, $mess);
        return back()->with('message','Thank you for your mesage, you shall hear from us shortly');

    }
}
