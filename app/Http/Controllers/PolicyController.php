<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Policy;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;

class PolicyController extends Controller
{
    public function index()
    {
    	$policy_all=Policy::orderBy('id','ASC')->take(6)->get();
      	return view('dashboard.policy',compact('policy_all'));
    }
    public function store(Requests\PolicyRequest $requests){
    	$post=new Policy;
    	$post_id=uniqid();
    	$post->policy_id=$post_id;
    	$post->policy_name=Input::get('policy_name');
    	//$post->say_comp=Input::get('say_comp');
    	$post->policy_content=Input::get('policy_content');
		$post->save();
        return back()->with('message','Policy Added Succesfully');
    }
    public function policySingle($id){
        $policysingle=Policy::where('policy_id',$id)->first();
        return view('page',compact('policysingle'));
    }

}
