<?php

namespace App\Http\Controllers;

use App\Comments;
use App\Contacts;
use App\Issues;
use App\Posts;
use App\Replies;
use App\Shika_advices;
use App\ShikaAdvices;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class IssuesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Comments::orderBy('id','desc')->get()->take(5);
        $side_posts = Posts::orderBy('id', 'DESC')->where('publish','1')->get()->take(4);
        $footer_blog = Posts::orderBy('id', 'DESC')->where('publish','1')->get()->take(2);
        return view('lets-talk',compact('comments','side_posts','footer_blog'));
    }
    public function signup()
    {
        $comments = Comments::orderBy('id','desc')->get()->take(5);
        $side_posts = Posts::orderBy('id', 'DESC')->where('publish','1')->get()->take(4);
        $footer_blog = Posts::orderBy('id', 'DESC')->where('publish','1')->get()->take(2);
        return view('signup',compact('comments','side_posts','footer_blog'));
    }
    public function signup_store(Requests\UsersRequest $request)
    {
        $user = new User();
        $user_id = randId();
        $user_id_ = $user_id;
        $user->user_id = $user_id;
        $user->fullname = Input::get('fullname');
        $user->email = Input::get('email');
        $user->password = Hash::make(Input::get('password'));
        $user->save();
        Session::put('user', $user_id_);
        Session::put('name', Input::get('fullname'));
        return redirect()->back()->with('error_m','Please login to proceed');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function IssueSigin(Requests\IssueSigninRequest $request)
    {
        $credentials = array(
            'email'=>Input::get('email'),
            'password'=>Input::get('password'),
        );
        if(Auth::attempt($credentials,true))
        {


            $user_info = User::where('email',Input::get('email'))->first();
            $user_id = $user_info->user_id;
            $name = $user_info->fullname;

            Session::put('user',$user_id);
            return redirect('lets-talk')->with('user_info',$user_info);
        }
        return back()->with('error_m','Incorrect credentials, Please try again');
    }
    public function inbox($id)
    {
        $inboxs = Replies::where('user_id',$id)->first();

        $reply = Replies::where('user_id',$id)->first();
        if($reply->read == 0){
            $reply->read = 1;
            $reply->save();
        }
        return view('inbox',compact('inboxs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\IssuesRequest $request)
    {
        $issue = new Issues();
        $issue->user_id = Input::get('user');
        $issue->contact = Input::get('contact');
        $issue->message = Input::get('message');
        $issue->save();
        return back()->with('message','Thank you for your message, you shall hear from us shortly');
    }
    public function messages()
    {
        $posts = Contacts::orderBy('id','desc')->get();
        return view('dashboard.messages',compact('posts'));
    }
    public function messageItem($id)
    {
        $post = Contacts::where('user_id',$id)->first();
        return view('dashboard.message-item',compact('post'));
    }
    public function message($id)
    {
        $user = Issues::where('user_id',$id)->first();
        return view('dashboard.message',compact('user'));
    }
    public function bulkMessages()
    {
        return view('dashboard.bulk-message');
    }
    public function replyStore(Requests\ReplyRequest $request)
    {
        $reply = new Replies();
        $reply->user_id = Input::get('user_id');
        $reply->title = Input::get('title');
        $reply->reply = Input::get('reply');
        $reply->save();
        return back()->with('message','Reply sent successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function DeleteMessageSingle($id)
    {
        $message = Contacts::find($id);
        $message->delete();
        return back()->with('message','Message deleted successfully');
    }
    public function Logout()
    {
        Auth::logout();
        return redirect('signup');
    }
}
