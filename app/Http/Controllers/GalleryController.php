<?php

namespace App\Http\Controllers;

use App\Galleries;
use App\Images;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = Images::orderBy('id','desc')->paginate(30);
        return view('gallery',compact('images'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_index()
    {
        $teams = Galleries::orderBy('id','desc')->with('connect_galleries')->get();
        return view('dashboard.gallery',compact('teams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function Album(Requests\AlbumRequest $request)
    {
        $album = new Galleries();
        $aid = randId();
        $album->gallery_id = $aid;
        $album->gallery_name = Input::get('album_name');
        $album->gallery_description = Input::get('album_description');

        if($album->save()){
            foreach(Input::file('album_image') as $key => $album_image) {

                $album_image_main = new Images();

                $album_image_main->gallery_id = $aid;

                $album_image_main->image = Images::uploadImage($request->file('album_image')[$key]);
                $album_image_main->save();
            }
        }
        return back()->with('message','success');



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function SecretPics()
    {
         $blogs = Images::orderBy('id','desc')->paginate(50);
        return view('secret-pics',compact('blogs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
