<?php

namespace App\Http\Controllers;

use App\Comments;
use App\Faq;
use App\Newsletter_mails;
use App\Newsletters;
use App\Contacts;
use App\Posts;
use App\Teams;
use App\Topics;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Postscategories;
use Illuminate\Support\Facades\Input;

class PagesController extends Controller
{
    public function __construct(Postscategories $category)
    {
        $this->category = $category;
    }
    public function about(){
        $teams = Teams::all();
       return view('about',compact('footer_blog','teams'));
    }
    public function team(){
        return view('team');
    }
    public function teamSingle($id){
        $team = Teams::where('team_id',$id)->first();
        return view('team-single',compact('team','side_posts','comments'));
    }
    public function services()
    {
        return view('services', compact('categories'));
    }
    public function contact()
    {
        return view('contact');
    }
  public function ContactStore(Requests\ContactRequests $request){
      $contact = new Contacts();

      $cid = randId();
      $contact->user_id = $cid;
      $contact->fullname=Input::get('fullname');
      $contact->email=Input::get('email');
      $contact->phone=Input::get('phone');
      $contact->comments=Input::get('message');

      $mgs = urlencode('Message From: '.Input::get('fullname').' Message: '. Input::get('message'));

      $d_number = urlencode('0244139937');

      $contact->save();
      //sendTextSOD($d_number,$mgs);

      return back()->with('message','Thank you for your message');;
  }

    public function faq()
    {
        $faqs = Faq::paginate(10);
        return view('faq',compact('faqs'));
    }

    public function topic(Requests\TopicRequest $request)
    {
        $topic = new Topics();
        $topic_id = randId();
        $topic->topic_id = $topic_id;
        $topic->fullname = Input::get('fullname');
        $topic->email = Input::get('email');
        $topic->topic = Input::get('topic');
        $topic->save();


        return back()->with('message','Topic submitted successfully');
    }

    public  function privacy(){
        return view('privacy');
    }

}
