<?php

namespace App\Http\Controllers;

use App\Posts;
use App\Search_term;
use App\Subscribers;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Requests\SearchRequest $request)
    {
        $search = stripslashes(Input::get('search'));
        $search_results = Posts::where('post_content', 'LIKE', '%'.$search.'%')->get();
        Session::put('search_results',$search_results);
        Session::put('search',$search);

        $s = new Search_term();
        $s->search_term = Input::get('search');
        $s->save();


        return redirect('search');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('search');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function search_terms()
    {
        $terms = Search_term::orderBy('id','desc')->get();
       return view('dashboard.search',compact('terms'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function SearchDelete($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function subscribers()
    {
        $terms = Subscribers::orderBy('id','desc')->get();
        return view('dashboard.subscribers',compact('terms'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function DeleteSearchTerms($id)
    {
        $search = Search_term::find($id);
        $search->delete();
        return back()->with('message','Search term deleted successfully');
    }
}
