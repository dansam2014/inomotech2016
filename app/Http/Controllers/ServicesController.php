<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Say;
use App\Http\Requests;

class ServicesController extends Controller
{
     public function index(){
     	$say_all = Say::orderBy('id','DESC')->take(4)->get();
    	return view('services',compact('say_all'));
    }
}
