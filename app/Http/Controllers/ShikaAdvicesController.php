<?php

namespace App\Http\Controllers;

use App\Comment_replies;
use App\Comments;
use App\Shika_advices;
use App\ShikaAdvices;
use App\Solutions;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class ShikaAdvicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('naa-shika-advices');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\NaaShikaRequest $request)
    {
        $advice = new Shika_advices();
        $user_id = randId();
        $advice->user_id = $user_id;
        $advice->fullname = Input::get('fullname');
        $advice->contact = Input::get('contact');
        $advice->email = Input::get('email');
        $advice->subject = Input::get('subject');
        $advice->message = Input::get('message');
        $advice->save();
        return back()->with('message','We have successfully received your message, you shall hear from us shortly');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function CommentsCmt()
    {
        $posts = Comment_replies::orderBy('id','desc')->get();
      return view('dashboard.commentsComment',compact('posts'));
    }
    public function inboxSingle($id){
        $posts = Shika_advices::where('user_id',$id)->get();
        return view('dashboard.inbox-single',compact('posts'));
    }

    public function OutboxView()
    {
        $posts = Shika_advices::with('connect_solutions')->get();
        return view('dashboard.outbox',compact('posts'));
    }
    public function outboxSingle($id){
        $posts = Solutions::with('Shika_advices')->where('user_id',$id)->get();
        return view('dashboard.outbox-single',compact('posts'));
    }
    public function outbox($id)
    {
        $posts = Shika_advices::where('user_id',$id)->first();
        return view('dashboard.outbox',compact('posts'));
    }
    public function reply($id)
    {
        $post = Shika_advices::where('user_id',$id)->first();
        return view('dashboard.reply',compact('post'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
/*    public function shikaReplies(Requests\ShikaRepliesRequest $request)
    {
        $reply = new Solutions();
        $reply->user_id = Input::get('user_id');
        $reply->subject = Input::get('subject');
        $reply->reply = Input::get('reply');
        $reply->publish = '1';
        $reply->save();
        return back()->with('message','Reply send successfully');
    }*/

    public function reply_store(Requests\ReplyRequest $request)
    {
        $solution = new Solutions();
        $solution->user_id = Input::get('user_id');
        $solution->subject = Input::get('subject');
        $solution->reply = Input::get('reply');
        $solution->publish = '1';
        $solution->save();
        return back()->with('message','Reply sent successfully');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        //return Input::all();

        $action = Input::get('action');
        if(Input::has('post')){
            switch ($action) {
                case 'publish':
                    foreach (Input::get('post') as $p) {
                        $posts = Shika_advices::find($p);
                        $posts->publish = 1;
                        $posts->save();
                    }
                    break;
                case 'unpublish':
                    foreach (Input::get('post') as $p) {
                        $posts = Shika_advices::find($p);
                        $posts->publish = 0;
                        $posts->save();
                    }
                    break;
                case 'delete':
                    foreach (Input::get('post') as $p) {
                        $posts = Shika_advices::find($p);
                        $posts->delete();

                    }
                    break;

                default:
                    # code...
                    break;
            }
        }
        return Redirect()->back();
    }

    public function DeleteComment()
    {
        //return Input::all();

        $action = Input::get('action');
        if(Input::has('post')){
            switch ($action) {
                case 'delete':
                    foreach (Input::get('post') as $p) {
                        $posts = Comment_replies::find($p);
                        $posts->delete();

                    }
                    break;

                default:
                    # code...
                    break;
            }
        }
        return Redirect()->back();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
