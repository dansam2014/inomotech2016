<?php

namespace App\Http\Controllers;

use App\Comments;
use App\Faq;
use App\Posts;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $faqs = Faq::orderBy('id', 'DESC')->get();
        return view('dashboard.faq',compact('faqs'));
    }
    public function faqView()
    {
        $comments = Comments::orderBy('id','desc')->get()->take(5);
        $side_posts = Posts::orderBy('id', 'DESC')->where('publish','1')->get()->take(4);
        $footer_blog = Posts::orderBy('id', 'DESC')->where('publish','1')->get()->take(2);
        $faqs = Faq::orderBy('id','desc')->paginate(10);
        return view('faq',compact('faqs','comments','side_posts','footer_blog'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Requests\FaqRequests $request)
    {
       $faq = new Faq();
        $faq->user_id = 12121320;
        $faq->faq_title = strtoupper(Input::get('faq_title'));
        $faq->faq_content = Input::get('faq_content');
        $faq->save();
        return redirect('faq')->with('message','FAQ added successfully');;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function viewFaqupdate($id)
    {
        $faq = Faq::where('id',$id)->first();
        return view('dashboard.faq-single',compact('faq'));
    }
    public function editFaq($id,Request $requests)
    {
        $inputs = $requests->all();
        $faq = Faq::find($id);
        $faq->update($inputs);
        return redirect('faq')->with('message','Faq edited successfully');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $faq = Faq::find($id);
        $faq->delete();

        return Redirect('faq')->with('message','Successfully deleted FAQ');
    }
}
