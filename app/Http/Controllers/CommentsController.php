<?php

namespace App\Http\Controllers;

use App\Comment_replies;
use App\Comments;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\CommentsRequest $request)
    {
        $comment = new Comments();
        $comment_id = randId();
        $comment->post_id = Input::get('post_id');
        $comment->comment_id = $comment_id;
        $comment->fullname = Input::get('fullname');
        $comment->email = Input::get('email');
        $comment->c_image =  Comments::uploadImage($request->file('c_image'));
        $comment->comment = Input::get('message');
        $comment->save();
        return back()->with('message','Thank you for your comment');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function CommentCommentStore()
    {
        $reply = new Comment_replies();
        $reply->comment_id = Input::get('comment_id');
        $reply->fullname = Input::get('fullname');
        $reply->comment = Input::get('message');
        $reply->save();
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
