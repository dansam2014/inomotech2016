<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.login');
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    public function signin(Requests\UserRequest $request)
    {
        $credentials = array(
            'email'=>Input::get('email'),
            'password'=>Input::get('password'),
        );
        if(Auth::attempt($credentials,true))
        {

            $user_info = User::where('email',Input::get('email'))->first();
            $user_id = $user_info->user_id;
            $name = $user_info->fullname;

            Session::put('user',$user_id);
            Session::put('name',$name);

            //$session_data = User::where('email',Input::get('email'))->with('connect_add_user')->get();


            return redirect('/');
        }else {
            return back()->with('error_message', 'Incorrect credentials, Please try again');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function ResetPassword()
    {
        return view('dashboard.reset');
    }
    public function ValidateReset(Requests\ValidateReset $reset)
    {
        $code = Input::get('code');

        $user = User::where('auth_token',$code)->first();

        $user->password = Hash::make(Input::get('password'));
        $user->save();
        return redirect('login')->with('error_message', 'Please Login with your username and password');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function Logout()
    {
        Auth::logout();
        return redirect('login');
    }
}
