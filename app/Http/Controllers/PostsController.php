<?php

namespace App\Http\Controllers;

use App\Comments;
use App\Posts;
use App\Postscategories;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $posts = Posts::orderBy('id','desc')->where('publish','1')->get();
        $categories = Postscategories::lists('category','slug');
      return view('dashboard.posts', compact('categories','posts'));
    }
    public function index1()
    {
        $posts = Posts::orderBy('id','desc')->where('publish','1')->get();
        $categories = Postscategories::lists('category','slug');
        return view('dashboard.posts1', compact('categories','posts'));
    }

    public function BlogCat($id){
        Session::put('caty',$id);
        $blogs = Posts::orderBy('id','desc')->where('publish','1')->where('category',$id)->paginate(12);
        return view('blogs-cat',compact('blogs'));

    }

    public function GenBlog(){
        $blogs = Posts::orderBy('id','desc')->where('publish','1')->paginate(12);
        return view('blogs',compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function FrontIndex()
    {

        $blogs = Posts::orderBy('id','desc')->where('publish','1')->paginate(12);

        return view('blogs-cat',compact('blogs'));
    }
    public function BlogSingleIndex($id){

        $blog = Posts::where('post_id',$id)->where('publish','1')->first();
        $bg = Posts::where('post_id',$id)->where('publish','1')->first();
        $comments = Comments::orderBy('id','desc')->with('comment_reply')->where('publish','1')->where('post_id',$blog->post_id)->get();
        return view('blog-single',compact('blog','comments','bg'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Requests\PostRequests $requests)
    {
        $post = new Posts();
        $post->post_id = randId();
        $post->category = Input::get('category');
        $post->post_title = Input::get('post_title');
        $post->post_content = Input::get('post_content');
        $post->post_image =  Posts::uploadImage($requests->file('post_image'));
        $post->schedule_date = Input::get('schedule_date');
        $post->save();

        $cat = Postscategories::all();

        return redirect('posts')->with('message','Post added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }
    public function postSingle($id)
    {
        $categories = Postscategories::lists('category','slug');
        $post = Posts::where('post_id',$id)->first();
        return view('dashboard.posts-single',compact('post','categories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function editPosts($id,Requests\PostRequests $requests)
    {
        $post = Posts::find($id);

        $inputs = $requests->all();

        if($post->update($inputs)){
            if(Input::file('post_image')){
                $post->post_image =  Posts::uploadImage($requests->file('post_image'));
            }
            $post->save();
        }

        return redirect('posts')->with('message','Post edited successfully');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update()
    {
       //return Input::all();

        $action = Input::get('action');
        if(Input::has('post')){
            switch ($action) {
                case 'publish':
                    foreach (Input::get('post') as $p) {
                        $posts = Posts::find($p);
                        $posts->publish = 1;
                        $posts->save();
                    }
                    break;
                case 'unpublish':
                    foreach (Input::get('post') as $p) {
                        $posts = Posts::find($p);
                        $posts->publish = 0;
                        $posts->save();
                    }
                    break;
                case 'delete':
                    foreach (Input::get('post') as $p) {
                        $posts = Posts::find($p);
                        $posts->delete();

                    }
                    break;

                default:
                    # code...
                    break;
            }
        }
        return Redirect()->back();
    }

    public function unpublishPosts()
    {
        //return Input::all();

        $action1 = Input::get('action1');
        if(Input::has('post1')){
            switch ($action1) {
                case 'publish':
                    foreach (Input::get('post1') as $p) {
                        $posts = Posts::find($p);
                        $posts->publish = 1;
                        $posts->save();
                    }
                    break;
                case 'delete':
                    foreach (Input::get('post1') as $p) {
                        $posts = Posts::find($p);
                        $posts->delete();

                    }
                    break;

                default:
                    # code...
                    break;
            }
        }
        return Redirect()->back();
    }

    public function unpublish()
    {
        $posts = Posts::where('publish','0')->orderBy('id','desc')->get();
        return view('dashboard.unpublish', compact('posts'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $cat2 = Postscategories::find($id);
        $cat2->delete();

        return Redirect('posts-category')->with('message','Successfully deleted category');

    }
    public function destroyPost($id)
    {
        $post = Posts::find($id);
        $post->delete();

        return Redirect('posts')->with('message','Successfully deleted post');

    }
    public function blogSingle()
    {

        return view('blogsingle');
    }
    public function categoryView()
    {
        $cats = \App\Postscategories::all();
        return view('dashboard.posts-category',compact('cats'));
    }
    public function postCategory(Requests\Postscategories $request)
    {
        $cat = new Postscategories();
        $cat->category = strtoupper(Input::get('category'));
        $cat->slug = strtolower(Input::get('category'));
        $cat->save();
        return redirect('posts-category');
    }

}
