<?php

namespace App\Http\Controllers;

use App\Comment_replies;
use App\Comments;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Posts;
use Illuminate\Support\Facades\Input;

class BlogController extends Controller
{
   public function index()
   {
       // $blogs1 = Posts::where('publish',1)->orderBy('id', 'DESC')->take(6)->get();
       $blogs = Posts::where('publish',1)->orderBy('id', 'DESC')->paginate(9);
       return view('blogs',compact('blogs','blogs1'));
   }


   public function show($id)
   {
   		$blog = Posts::with(array('comments' => function($query){
			   			$query->with('replies')
			   			->where('parent_id',null)->orderBy('id','desc');
			   		}))
   					->where('post_id',$id)->first();
   		return view('blogsingle', compact('blog'));
   }
    public function blogSingle($id)
    {
        $blogs1 = Posts::where('publish',1)->orderBy('id', 'DESC')->take(6)->get();
//        $comments = Comments::with('comment_reply')->where('post_id',$id)->get();
        $post = Posts::where('post_id',$id)->first();
        return view('blog-single',compact('blogs1','post'));
    }
    public function blog($id)
    {
        $footer_blog = Posts::orderBy('id', 'DESC')->where('publish','1')->get()->take(2);
        $blogs = Posts::where('publish',1)->orderBy('id', 'DESC')->paginate(9);
        return view('blogs',compact('blogs','footer_blog'));
    }
   
    public function storeReply(Request $request)
    {
        $comment = new Comment_replies();
        $comment->fullname = Input::get('fullname');
        $comment->comment_id =Input::get('comment_id');
        $comment->reply = Input::get('reply');
        $comment->save();
        return back()->with('message','Thank you for your comment');
    }
}
