<?php

namespace App\Http\Controllers;

use App\Contacts;
use App\Posts;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$footer_blog = Posts::orderBy('id', 'DESC')->where('publish','1')->get()->take(2);
        return view('contact');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Requests\ContactRequests $requests)
    {
        $contact = new Contacts();
        $contact->fullname = Input::get('fullname');
        $contact->email = Input::get('email');
        $contact->phone = Input::get('phone');
        $contact->message = Input::get('message');
        $contact->save();
        return back()->with('message','Thank you for your message, you shall hear from us shortly');
    }

    public function store_from_contact_page(Requests\ContactRequests $requests)
    {
        $contact = new Contacts();
        $contact->fullname = Input::get('fullname');
        $contact->email = Input::get('email');
        $contact->phone = Input::get('phone');
        $contact->subject = Input::get('subject');
        $contact->message= Input::get('message');
        $contact->save();
        return back()->with('message','Thank you for your message, you shall hear from us shortly');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
