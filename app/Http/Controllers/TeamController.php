<?php

namespace App\Http\Controllers;

use App\PositionCategories;
use App\Positioncats;
use App\Teams;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $teams = Teams::orderBy('id','desc')->get();
        $position = Positioncats::lists('position','slug');
        //return view('dashboard.posts', compact('categories'));
        return view('dashboard.team',compact('position','teams'));
    }
    public function about(){
        $team = Teams::orderBy('id','ASC')->take(4)->get();
        return view('about',compact('team'));
    }
    public function categoryView()
    {
        $positions = Positioncats::orderBy('id','desc')->get();
        return view('dashboard/team-category',compact('positions'));
    }
    public function positionCat(Requests\PositionRequest $request)
    {
        $cat = new Positioncats();
        $cat->position = strtoupper(Input::get('position'));
        $cat->slug = strtolower(Input::get('position'));
        $cat->save();
        return redirect('team-category');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }
    public function teamSingle($id)
    {
    $teams = Teams::where('team_id',$id)->get();
        return view('team-single',compact('teams'));
    }
    public function teamIndex()
    {
        $teams = Teams::all();
        return view('teams',compact('teams'));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Requests\TeamsRequest $request)
    {
        $team = new Teams();
        $t_id = randId();
        $team->team_id = $t_id;
        $team->fullname = Input::get('fullname');
        $team->position = Input::get('position');
        $team->biography = Input::get('biography');
        $team->team_image =  Teams::uploadImage($request->file('team_image'));
        $team->facebook = Input::get('facebook');
        $team->twitter = Input::get('twitter');
        $team->save();
        return redirect('team')->with('message','Successfully add team member');

    }

    public function teamSingl($id)
    {
        $team = Teams::where('team_id',$id)->first();
        return view('dashboard.team-single',compact('team'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
//    public function editTeam(Requests\TeamsRequest $request, $id)
//    {
//        $team = Teams::find($id);
//        $inputs = $request->all();
//        if($team->update($inputs)){
//            $team->team_image =  Teams::uploadImage($request->file('team_image'));
//            $team->save();
//        }
//        return redirect('team')->with('message','Team edited successfully');
//
//    }

    public function editTeam(Request $request, $id)
    {
        $team = Teams::find($id);
        if(Input::get('fullname')){
            $team->fullname = Input::get('fullname');
        }if(Input::get('position')) {
        $team->position = Input::get('position');
        }if(Input::get('biography')) {
        $team->biography = Input::get('biography');
        }if(Input::get('facebook')) {
        $team->facebook = Input::get('facebook');
        }if(Input::get('twitter')) {
        $team->twitter = Input::get('twitter');
        }if(Input::file($team->team_image)) {
        $team->team_image =  Teams::uploadImage($request->file('team_image'));
        }
        $team->save();

        return redirect('team')->with('message','Team edited successfully');

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $position = Positioncats::find($id);
        $position->delete();

        return Redirect('team-category')->with('message','Successfully deleted category');
    }
    public function destroyTeams($id)
    {
        $team = Teams::find($id);
        $team->delete();

        return Redirect('team')->with('message','Successfully deleted team member');
    }
}
