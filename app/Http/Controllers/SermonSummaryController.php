<?php

namespace App\Http\Controllers;

use App\Comments;
use App\Sermon_summary;
use App\Summary_comments;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class SermonSummaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sermon = Sermon_summary::orderBy('id','desc')->where('publish','1')->paginate(12);
        return view('sermon-summary',compact('sermon'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function adminSermon()
    {
        $sermon = Sermon_summary::orderBy('id','desc')->where('publish','1')->get();
        return view('dashboard.sermon-summary',compact('sermon'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\SermonSummaryRequest $request)
    {
        $sermon = new Sermon_summary();
        $sid = randId();
        $sermon->summary_id = $sid;
        $sermon->summary_title = Input::get('sermon_title');
        $sermon->preacher = Input::get('preacher');
        $sermon->summary_content = Input::get('summary_content');
        $sermon->pic =  Sermon_summary::uploadImage($request->file('pic'));
        $sermon->sermon_date = Input::get('sermon_date');
        $sermon->save();
        return back()->with('message','Sermon summary added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function SingleSummary($id)
    {
        $blog = Sermon_summary::where('summary_id',$id)->where('publish','1')->first();
        $comments = Summary_comments::orderBy('id','desc')->where('summary_id',$blog->summary_id)->get();
        return view('summary-single',compact('blog','comments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function Edit($id)
    {
        $sermon = Sermon_summary::orderBy('id','desc')->where('summary_id',$id)->first();
        return view('dashboard.sermon-summary-edit',compact('sermon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function EditStore(Request $request)
    {
       $id = Input::get('summary_id');

        $sermon = Sermon_summary::find($id);

        if(Input::get('sermon_title')){
            $sermon->summary_title = Input::get('sermon_title');
        }if(Input::get('preacher')){
        $sermon->preacher = Input::get('preacher');
        }if(Input::get('sermon_date')){
        $sermon->sermon_date = Input::get('sermon_date');
        }if(Input::get('summary_content')){
        $sermon->summary_content = Input::get('summary_content');
        }if(Input::file('pic')){
        $sermon->pic =  Sermon_summary::uploadImage($request->file('pic'));
        }
        $sermon->save();
        return back()->with('message','Sermon summary edited successfully');
    }
    public function SummaryComment(){

        $comment_id = randId();
        $comment = new Summary_comments();
        $comment->summary_id = Input::get('summary_id');
        $comment->comment_id = $comment_id;
        $comment->fullname = Input::get('fullname');
        $comment->email = Input::get('email');
        $comment->comment = Input::get('comment');
        $comment->save();
        return back()->with('message','Thank you for your comment');
    }

    public function CommentSummaryAdmin(){
        $posts = Summary_comments::orderBy('id','desc')->get();
        return view('dashboard.summaryComment',compact('posts'));
    }

    public function DeleteSummaryComment(){
        $action = Input::get('action');
        if(Input::has('post')){
            switch ($action) {
                case 'delete':
                    foreach (Input::get('post') as $p) {
                        $posts = Summary_comments::find($p);
                        $posts->delete();

                    }
                    break;

                default:
                    # code...
                    break;
            }
        }
        return Redirect()->back()->with('message','Comment(s) deleted successfully');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
