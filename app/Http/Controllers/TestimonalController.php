<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Say;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;

class TestimonalController extends Controller
{
    public function index()
    {
		$say_content = Say::latest('created_at')->get();
        return view('dashboard.say',compact('say_content'));
    }
  	public function store(Requests\SayRequest $requests){
    	$post=new Say;
    	$post_image = Say::uploadImage($requests->file('say_image'));
    	$post_id=uniqid();
    	$post->say_id=$post_id;
    	$post->say_name=Input::get('say_name');
    	//$post->say_comp=Input::get('say_comp');
    	$post->say_content=Input::get('say_content');
    	$post->say_image=$post_image;
		$post->save();
        return back()->with('message','Testimonial Added Succesfully');
    }
    public function delete_say($id){
        $delete=Say::find($id);
        $delete->delete();
        return back()->with('message','Testimonial deleted successfully');
    }
     public function edit_say($id){
        $edit_say=Say::find($id);
        return view('admin.editsay',compact('edit_say'));
    }
    public function update_say($id,Requests\SaysRequest $requests){
        $post = Say::find($id);
        $inputs = $requests->all();
     if($post->update($inputs)){
            if(Input::file('say_image')){
                $post->say_image = Say::uploadImage($requests->file('say_image'));
            }
            $post->save();
        }
        return back()->with('message','Testimonial edited Successfully');
    }
}


