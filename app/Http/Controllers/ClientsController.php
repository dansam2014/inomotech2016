<?php

namespace App\Http\Controllers;

use App\ClientCategories;
use App\Clients;
use App\Say;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ClientsController extends Controller
{
    public function index(){
        $say_all = Say::orderBy('id','DESC')->take(4)->get();
        $clients = DB::table('clients')->take(6)->where('category','website')->orderBy('priority','desc')->get();
        $webApp = DB::table('clients')->where('category','web app')->orderBy('priority','desc')->paginate(16);
        $webs = DB::table('clients')->orderBy('priority','desc')->paginate(32);
    	return view('clients',compact('clients','webApp','webs','say_all'));
    }

    public function cientCat(){
        $cats = ClientCategories::orderBy('id','desc')->get();
    	return view('dashboard.client-category',compact('cats'));
    }
    public function cientCatPosts(Requests\ClientCategoryRequest $request){

        $cat = new ClientCategories();
        $cat->slug = strtolower(Input::get('category'));
        $cat->category = strtoupper(Input::get('category'));
        $cat->save();
        return back()->with('message',' Category added successfully');
    }
    public function delCategory($id){
        $cat =ClientCategories::find($id);
        $cat->delete();
        return back()->with('message','Category deleted successfully');
    }
    public function clientView(){
        $clients = Clients::orderBy('id','desc')->get();
        $categories = ClientCategories::lists('category','slug');
        return view('dashboard.clients',compact('categories','clients'));
    }
    public function clientSave(Requests\ClientRequest $request){
        $client = new Clients();
        $client->client_id = randId();
        $client->name = Input::get('name');
        $client->category = Input::get('category');
        $client->description = Input::get('description');
        $client->client_image =  Clients::uploadImage($request->file('client_image'));
        $client->client_logo =  Clients::uploadImage($request->file('client_logo'));
        $client->link = Input::get('link');
        $client->priority = Input::get('priority');
        $client->save();
        return back()->with('message','Client saved successfully');

    }

    public function clientSingle($id){
        $client = Clients::where('client_id',$id)->first();
        return view('client-single',compact('client'));
    }
    public function ClientSingleAdmin($id){
        $team = Clients::where('client_id',$id)->first();
        return view('dashboard.client-single',compact('team'));
    }
    public function ClientEdit(Request $request){
         $client_id = Input::get('client_id');

         $client = Clients::find($client_id);

        if(Input::get('name')){
            $client->name = Input::get('name');
        }if(Input::get('category')){
            $client->category = Input::get('category');
        }if(Input::get('description')){
            $client->description = Input::get('description');
        }if(Input::get('link')){
            $client->link = Input::get('link');
        }if(Input::get('priority')){
            $client->priority = Input::get('priority');
        }if(Input::file('client_image')){
            $client->client_image =  Clients::uploadImage($request->file('client_image'));
        }if(Input::file('client_logo')){
            $client->client_logo =  Clients::uploadImage($request->file('client_logo'));
        }
        $client->save();

        return back()->with('message','Client updated successfully');
    }

    public function ClientDeletion($id){
        $client = Clients::find($id);
        $client->delete();
        return back()->with('message','Client delted successfully');
    }

}
