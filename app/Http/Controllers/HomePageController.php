<?php

namespace App\Http\Controllers;

use App\Posts;
use App\Subscribers;
use Illuminate\Http\Request;
use App\Say;
use App\Clients;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;

class HomePageController extends Controller
{
    public function index(){
        $posts = Posts::orderBy('id','desc')->where('publish','1')->take(2)->get();
    	$say_all = Say::orderBy('id','DESC')->take(4)->get();
    	$clients = DB::table('clients')->take(6)->where('category','website')->orderBy('priority','desc')->get();
        return view('index',compact('say_all','clients','posts'));
    }
    public function index1(){
        $posts = Posts::orderBy('id','desc')->where('publish','1')->take(2)->get();
    	$say_all = Say::orderBy('id','DESC')->take(4)->get();
    	$clients = DB::table('clients')->take(6)->where('category','website')->orderBy('priority','desc')->get();
        return view('index',compact('say_all','clients','posts'));
    }
    public function Newsletter(Requests\NewsletterRequest $request)
    {
        $news = new Subscribers();
        $n = randId();
        $news->user_id = $n;
        $news->email = Input::get('email');
        $news->save();
        return back()->with('message','You have successfully signed up');
    }
}
