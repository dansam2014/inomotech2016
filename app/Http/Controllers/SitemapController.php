<?php

namespace App\Http\Controllers;

use App\Couple_registrations;
use App\Events;
use App\Policy;
use App\Posts;
use App\Sermon_summary;
use App\Speakers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Response;
use Illuminate\View\View;

class SitemapController extends Controller
{
    public function sitemap() {

        $posts = Posts::where('publish','1')->get();
        $pages = Policy::orderBy('id','desc')->get();

        return Response::view('sitemap', compact('pages','posts'))
            ->header('Content-Type', 'application/xml');
    }
}
