<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContactRequests extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullname'=>'required|max:70',
            'phone'=>'required|min:9|max:20',
            'email'=>'email|min:7|max:30',
            'message'=>'required'
        ];
    }
}
