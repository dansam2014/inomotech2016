<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NaaShikaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullname'=>'required | max:70',
            'contact'=>'required | max:20',
            'email'=>'email|max:30',
            'subject'=>'required',
            'message'=>'required'
        ];
    }
}
