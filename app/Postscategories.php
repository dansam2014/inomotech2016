<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postscategories extends Model
{
    public function postCount()
    {
        return $this->hasOne('App\Posts','category','slug')->selectRaw('count(*)');
    }
}
