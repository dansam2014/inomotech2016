<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solutions extends Model
{
    protected $primaryKey = "user_id";
    public function Shika_advices()
    {
        return $this->belongsTo('App\Shika_advices', 'user_id', 'user_id');
    }
}
