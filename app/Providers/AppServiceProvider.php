<?php

namespace App\Providers;
use App\Posts;
use Illuminate\Support\Facades\DB;
use App\Clients;
use App\Policy;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $client_1 = DB::table('clients')->take(6)->orderBy('priority','desc')->get();
        View()->share('client_1',$client_1);

        $blogs1 = Posts::where('publish',1)->orderBy('id', 'DESC')->take(6)->get();
        View()->share('blogs1',$blogs1);

        $policy = Policy::orderBy('id','ASC')->take(6)->get();
        View()->share('policy',$policy);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
