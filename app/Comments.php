<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    public static function uploadImage($file)
    {
        if (! empty($file)) {
            $ext  = $file->getClientOriginalExtension();
            $dir = public_path().'/uploads/comments/';
            $path = '/uploads/comments/';

            $filename = uniqid();
            $uploadedfile = $filename.".{$ext}";

            if($file->move($dir, $uploadedfile)){
                return $path.$uploadedfile;
            }
        }
        return '';
    }
    public function comment_reply()
    {
        return $this->hasMany('App\Comment_replies', 'comment_id', 'comment_id');
    }
}
