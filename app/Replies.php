<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Replies extends Model
{
    protected  $primaryKey = 'user_id';
    function connect_user()
    {
        $this->hasMany('App\User','user_id','user_id');
    }

    function users()
    {
        $this->belongsTo('App\User','user_id','user_id');
    }
}
